define(['require','shared/toaster','modules/settings_cache','modules/dialog_service','dsds_settings','shared/settings_helper'],function(require) {
  

  /**
   * Singleton object that handles some call settings.
   */
  var Toaster = require('shared/toaster');
  var SettingsCache = require('modules/settings_cache');
  var DialogService = require('modules/dialog_service');
  var DsdsSettings = require('dsds_settings');
  var SettingsHelper = require('shared/settings_helper');

  var CallSettings = function cs_callsettings() {
    var _networkTypeCategory = {
      'gprs': 'gsm',
      'edge': 'gsm',
      'umts': 'gsm',
      'hsdpa': 'gsm',
      'hsupa': 'gsm',
      'hspa': 'gsm',
      'hspa+': 'gsm',
      'lte': 'gsm',
      'gsm': 'gsm',
      'is95a': 'cdma',
      'is95b': 'cdma',
      '1xrtt': 'cdma',
      'evdo0': 'cdma',
      'evdoa': 'cdma',
      'evdob': 'cdma',
      'ehrpd': 'cdma'
    };

    var _cfReason = {
      CALL_FORWARD_REASON_UNCONDITIONAL: 0,
      CALL_FORWARD_REASON_MOBILE_BUSY: 1,
      CALL_FORWARD_REASON_NO_REPLY: 2,
      CALL_FORWARD_REASON_NOT_REACHABLE: 3
    };
    var _cfReasonMapping = {
      'unconditional': _cfReason.CALL_FORWARD_REASON_UNCONDITIONAL,
      'mobilebusy': _cfReason.CALL_FORWARD_REASON_MOBILE_BUSY,
      'noreply': _cfReason.CALL_FORWARD_REASON_NO_REPLY,
      'notreachable': _cfReason.CALL_FORWARD_REASON_NOT_REACHABLE
    };
    var _cfAction = {
      CALL_FORWARD_ACTION_DISABLE: 0,
      CALL_FORWARD_ACTION_ENABLE: 1,
      CALL_FORWARD_ACTION_QUERY_STATUS: 2,
      CALL_FORWARD_ACTION_REGISTRATION: 3,
      CALL_FORWARD_ACTION_ERASURE: 4
    };

    var _clirConstantsMapping = {
      'CLIR_DEFAULT': 0,
      'CLIR_INVOCATION': 1,
      'CLIR_SUPPRESSION': 2
    };

    var _settings = window.navigator.mozSettings;
    var _mobileConnections = window.navigator.mozMobileConnections;
    var _voiceTypes = Array.prototype.map.call(_mobileConnections,
      function() { return null; });

    /** mozMobileConnection instance the panel settings rely on */
    var _mobileConnection = null;
    var _imsHandler = null;
    /**
      * To prevent current panle frequently refresh
      *   while the voice type is changing.
      *
      * @param {String} 'cdma' or 'gsm'
      */
    var _currentType = null;
    /** Voice service class mask */
    var _voiceServiceClassMask = null;
    /** Stores current states (enabler or not) of the call forwaring reason */
    var _cfReasonStates = [0, 0, 0, 0];
    /** Flag */
    var _ignoreSettingChanges = false;
    /** Flag */
    var _getCallForwardingOptionSuccess = true;
    /** Task scheduler */
    var _taskScheduler = null;

    /** @HACK If devices is querying Call Waiting status,
      * should disable the select option.
      */
    var _callWaitingQueryStatus = false;

    /**
     * Init function.
     */
    function cs_init() {
      // Get the mozMobileConnection instace for this ICC card.
      _mobileConnection = _mobileConnections[
        DsdsSettings.getIccCardIndexForCallSettings()
      ];
      if (!_mobileConnection) {
        return;
      }

      _voiceServiceClassMask = _mobileConnection.ICC_SERVICE_CLASS_VOICE;
      _taskScheduler = TaskScheduler();

      cs_checkNetworkType();
      // Init call setting stuff.
      cs_initVoicePrivacyMode();
      cs_initCallWaiting();
      cs_initCallerId();
      cs_initFdnItem();

      // Update items in the call settings panel.
      window.addEventListener('panelready', function(e) {
        // Get the mozMobileConnection instace for this ICC card.
        _mobileConnection = _mobileConnections[
          DsdsSettings.getIccCardIndexForCallSettings()
        ];
        if (!_mobileConnection) {
          return;
        }

        switch (e.detail.current) {
          case '#call':
            // No need to refresh the call settings items if navigated from
            // panels not manipulating call settings.
            if (e.detail.previous === '#call-cfSettings' ||
                e.detail.previous === '#call-cbSettings') {
              return;
            }

            cs_updateNetworkTypeLimitedItemsVisibility(_currentType);
            cs_refreshCallSettingItems();
            break;
          case '#call-cfSettings':
            // No need to refresh the call forwarding general settings
            // if navigated from panels not manipulating them.
            if (e.detail.previous.startsWith('#call-cf-')) {
              return;
            }
            cs_updateCallForwardingSubpanels();
            break;
        }
      });

      // We need to refresh call setting items as they can be changed in dialer.
      document.addEventListener('visibilitychange', function() {
        if (document.hidden) {
          return;
        }

        switch (Settings.currentPanel) {
          case '#call':
            cs_updateNetworkTypeLimitedItemsVisibility(_currentType);
            cs_refreshCallSettingItems();
            break;
          case '#call-cfSettings':
            cs_updateCallForwardingSubpanels();
            break;
        }
      });

      cs_initCallForwardingObservers();

      window.navigator.mozSettings.addObserver('phone.dtmf.type', () => {
        showToast('changessaved');
      });
    }

    function cs_initFdnItem() {
      var iccObj = getIccByIndex();
      var fdnMenuItem = document.getElementById('menuItem-callFdn');
      iccObj.getServiceState('fdn').then(function(hasFdn) {
        fdnMenuItem.hidden = !hasFdn;
      });
    }

    function showToast(msgId) {
      var toast = {
        messageL10nId: msgId,
        latency: 3000,
        useTransition: true
      };
      Toaster.showToast(toast);
    }

    function cs_checkNetworkType() {
      var voice = _mobileConnection.voice;
      var data = _mobileConnection.data;
      _imsHandler = _mobileConnection.imsHandler;
      if (_imsHandler &&
        (_imsHandler.capability === 'voice-over-wifi' ||
        _imsHandler.capability === 'video-over-wifi')) {
        _currentType = _imsHandler.capability;
        cs_updateNetworkTypeLimitedItemsVisibility(_currentType);
      } else if (voice && voice.state === 'registered' &&
        voice.connected === true) {
        _currentType = _networkTypeCategory[voice.type];
        cs_updateNetworkTypeLimitedItemsVisibility(_currentType);
      } else if (data && data.state === 'registered' &&
        data.connected === true) {
        _currentType = _networkTypeCategory[data.type];
        cs_updateNetworkTypeLimitedItemsVisibility(_currentType);
      }
    }

    function cs_addNetworkTypeCheckListener() {
      if (_mobileConnection.voice) {
        _mobileConnection.addEventListener('voicechange', cs_onTypeChange);
      }
      if (_mobileConnection.data) {
        _mobileConnection.addEventListener('datachange', cs_onTypeChange);
      }
      if (_imsHandler) {
        _imsHandler.addEventListener('capabilitychange',
          cs_onCapabilityChange);
      }
    }

    function cs_onTypeChange(evt) {
      var voiceType = _mobileConnection.voice && _mobileConnection.voice.type;
      var dataType = _mobileConnection.data && _mobileConnection.data.type;
      if (!voiceType && !dataType) {
        return;
      }
      var newType = _networkTypeCategory[voiceType || dataType];
      if (newType === _currentType) {
        return;
      } else {
        _currentType = newType;
      }
      cs_updateNetworkTypeLimitedItemsVisibility(newType);
      cs_refreshCallSettingItems();
    }

    function cs_onCapabilityChange() {
      if (_imsHandler.capability === 'voice-over-wifi' ||
        _imsHandler.capability === 'video-over-wifi') {
        if (_imsHandler.capability === _currentType) {
          return;
        } else {
          _currentType = newType;
        }
        cs_updateNetworkTypeLimitedItemsVisibility(_imsHandler.capability);
        cs_refreshCallSettingItems();
      }
    }

    /**
     * Update the network type limited items' visibility based on the
     * voice type or data type.
     */
    function cs_updateNetworkTypeLimitedItemsVisibility(newType) {
      // The following features are limited to GSM types.
      var callForwardingItem =
        document.getElementById('menuItem-callForwarding');
      var callBarringItem =
        document.getElementById('menuItem-callBarring');

      var callWaitingItem = document.getElementById('menuItem-callWaiting');
      var callerIdItem = document.getElementById('menuItem-callerId');
      // The following feature is limited to CDMA types.
      var voicePrivacyItem =
        document.getElementById('menuItem-voicePrivacyMode');

      callForwardingItem.hidden =
      callBarringItem.hidden =
      callWaitingItem.hidden =
      callerIdItem.hidden =
        (newType !== 'gsm' && newType !== 'voice-over-wifi' &&
        newType !== 'video-over-wifi');

      voicePrivacyItem.hidden =
        (newType !== 'cdma');

      window.dispatchEvent(new CustomEvent('refresh'));
    }

    function cs_removeNetworkTypeCheckListener() {
      if (_mobileConnection.voice) {
        _mobileConnection.removeEventListener('voicechange', cs_onTypeChange);
      }
      if (_mobileConnection.data) {
        _mobileConnection.removeEventListener('datachange', cs_onTypeChange);
      }
      if (_imsHandler) {
        _imsHandler.removeEventListener('capabilitychange', cs_onCapabilityChange);
      }
    }

    /**
     * Refresh the items in the call setting panel.
     */
    function cs_refreshCallSettingItems() {
      return new Promise((resolve) => {
        cs_updateFdnStatus();
        cs_updateVoicePrivacyItemState();

        cs_updateCallerIdPreference();
        cs_updateCallerIdItemState();
        cs_updateCallWaitingItemState();
        resolve();
      });
    }

    /**
     * Helper function. Check whether the phone number is valid or not.
     *
     * @param {String} number The phone number to check.
     *
     * @return {Boolean} Result.
     */
    function cs_isPhoneNumberValid(number) {
      if (number) {
        var re = /^([\+]*[0-9])+$/;
        if (re.test(number)) {
          return true;
        }
      }
      return false;
    }

    /**
     * Helper function. Stores settings into the database.
     */
    function cs_setToSettingsDB(settingKey, value, callback) {
      var done = function done() {
        if (callback) {
          callback();
        }
      };

      var getLock = _settings.createLock();
      var request = getLock.get(settingKey);
      request.onsuccess = function getFromDBSuccess() {
        var currentValue = request.result[settingKey];
        if (currentValue !== value) {
          var setLock = _settings.createLock();
          var cset = {};
          cset[settingKey] = value;
          var setRequest = setLock.set(cset);
          setRequest.onsuccess = done;
          setRequest.onerror = done;
        } else {
          done();
        }
      };
      request.onerror = done;
    }

    /**
     * Helper function. Displays rule info.
     */
    function cs_displayRule(rules, elementId) {
      var element = document.getElementById(elementId);

      element.innerHTML = '';
      for (var i = 0; i < rules.length; i++) {
        if (rules[i].active &&
            ((_voiceServiceClassMask & rules[i].serviceClass) != 0)) {
          navigator.mozL10n.setAttributes(element,
            'callForwardingForwardingVoiceToNumber',
            { number: rules[i].number });
          return;
        }
      }

      element.setAttribute('data-l10n-id', 'callForwardingNotForwarding');
    }

    /**
     * Helper function. Enables/disables tapping on call forwarding entry.
     */
    function cs_enableTapOnCallForwardingItems(enable) {
      // Update 'Call Forwarding' submenu items
      var elementIds = ['li-cfu-desc',
                        'li-cfmb-desc',
                        'li-cfnrep-desc',
                        'li-cfnrea-desc'];
      var hrefList = {
        'li-cfu-desc': '#call-cf-unconditionalSettings',
        'li-cfmb-desc': '#call-cf-mobileBusySettings',
        'li-cfnrep-desc': '#call-cf-noReplySettings',
        'li-cfnrea-desc': '#call-cf-notReachableSettings'
      };

      var isUnconditionalCFOn = (_cfReasonStates[0] === 1);

      elementIds.forEach(function(id) {
        var element = document.getElementById(id);
        if (!element) {
          return;
        }

        var hrefItem = element.querySelector('a');
        if (enable) {
          // @HACK To make user can't enter any page
          // when the devices get the SIM card state.
          hrefItem.setAttribute('data-href', hrefList[id]);
          element.removeAttribute('aria-disabled');
          // If unconditional call forwarding is on we keep disabled the other
          // panels.
          if (isUnconditionalCFOn && id !== 'li-cfu-desc') {
            hrefItem.removeAttribute('data-href');
            element.setAttribute('aria-disabled', true);
          }
        } else {
          hrefItem.removeAttribute('data-href');
          element.setAttribute('aria-disabled', true);
        }
      });
    }

    /**
     * Display information relevant to the SIM card state.
     */
    function cs_displayInfoForAll(l10nId) {
      var elementIds = ['cfu-desc', 'cfmb-desc', 'cfnrep-desc', 'cfnrea-desc'];
      elementIds.forEach(function(id) {
        var element = document.getElementById(id);

        // Clear all child elements before setting the l10n id
        element.innerHTML = '';
        element.setAttribute('data-l10n-id', l10nId);
      });
    }

    /**
     * Gets current call forwarding rules.
     */
    function cs_getCallForwardingOption(callback) {
      var onerror = function call_getCWOptionError() {
        if (callback) {
          _ignoreSettingChanges = false;
          callback(null);
        }
      };

      // Queries rules for unconditional call forwarding.
      var unconditional = _mobileConnection.getCallForwardingOption(
        _cfReason.CALL_FORWARD_REASON_UNCONDITIONAL);

      unconditional.onsuccess = function() {
        var unconditionalRules = unconditional.result;

        // Queries rules for call forwarding when device busy.
        var mobileBusy = _mobileConnection.getCallForwardingOption(
          _cfReason.CALL_FORWARD_REASON_MOBILE_BUSY);

        mobileBusy.onsuccess = function() {
          var mobileBusyRules = mobileBusy.result;

          // Queries rules for call forwarding when device does not reply.
          var noReply = _mobileConnection.getCallForwardingOption(
            _cfReason.CALL_FORWARD_REASON_NO_REPLY);

          noReply.onsuccess = function() {
            var noReplyRules = noReply.result;

            // Queries rules for call forwarding when device is not reachable.
            var notReachable = _mobileConnection.getCallForwardingOption(
              _cfReason.CALL_FORWARD_REASON_NOT_REACHABLE);

            notReachable.onsuccess = function() {
              var notReachableRules = notReachable.result;

              var cfOptions = {
                'unconditional': unconditionalRules,
                'mobilebusy': mobileBusyRules,
                'noreply': noReplyRules,
                'notreachable': notReachableRules
              };

              // Waits for all DB settings completed.
              var asyncOpChecker = {
                taskCount: 0,
                runTask: function(func) {
                  this.taskCount++;
                  var newArgs = [];
                  for (var i = 1; i < arguments.length; i++) {
                    newArgs.push(arguments[i]);
                  }
                  newArgs.push(this.complete.bind(this));
                  func.apply(window, newArgs);
                },
                complete: function() {
                  this.taskCount--;
                  if (this.taskCount === 0) {
                    this.finish();
                  }
                },
                finish: function() {
                  setTimeout(function() {
                    _ignoreSettingChanges = false;
                    callback(cfOptions);
                  }, 500);
                }
              };

              // While storing the settings into the database we avoid observing
              // changes on those ones and enabling/disabling call forwarding.
              _ignoreSettingChanges = true;
              // Ensures the settings being set to the setting DB.
              Object.keys(cfOptions).forEach(function(settingKey) {
                var rules = cfOptions[settingKey];
                var hasValidRule = false;
                for (var i = 0; i < rules.length; i++) {
                  if (rules[i].active &&
                    ((_voiceServiceClassMask & rules[i].serviceClass) != 0)) {
                    _cfReasonStates[_cfReasonMapping[settingKey]] = 1;
                    asyncOpChecker.runTask(
                      cs_setToSettingsDB,
                      'ril.cf.' + settingKey + '.number',
                      rules[i].number
                    );
                    asyncOpChecker.runTask(
                      cs_setToSettingsDB,
                      'ril.cf.' + settingKey + '.enabled',
                      true
                    );
                    if (settingKey === 'unconditional') {
                      // Send the latest query result from carrier to system app
                      asyncOpChecker.runTask(
                        cs_setToSettingsDB,
                        'ril.cf.carrier.enabled',
                        {
                          enabled: true,
                          index: DsdsSettings.getIccCardIndexForCallSettings()
                        }
                      );
                    }
                    hasValidRule = true;
                    break;
                  }
                }

                if (!hasValidRule) {
                  _cfReasonStates[_cfReasonMapping[settingKey]] = 0;
                  // Send the latest query result from carrier to system app
                  asyncOpChecker.runTask(
                    cs_setToSettingsDB,
                    'ril.cf.' + settingKey + '.number',
                    ''
                  );
                  asyncOpChecker.runTask(
                    cs_setToSettingsDB,
                    'ril.cf.' + settingKey + '.enabled',
                    false
                  );
                  if (settingKey === 'unconditional') {
                    asyncOpChecker.runTask(
                      cs_setToSettingsDB,
                      'ril.cf.carrier.enabled',
                      {
                        enabled: false,
                        index: DsdsSettings.getIccCardIndexForCallSettings()
                      }
                    );
                  }
                }
              });
            };
            notReachable.onerror = onerror;
          };
          noReply.onerror = onerror;
        };
        mobileBusy.onerror = onerror;
      };
      unconditional.onerror = onerror;
    }

    /**
     *
     */
    function cs_initCallForwardingObservers() {
      var settingKeys = ['unconditional',
                         'mobilebusy',
                         'noreply',
                         'notreachable'];
      settingKeys.forEach(function(key) {
        _settings.addObserver('ril.cf.' + key + '.enabled', function(event) {
          // While storing the settings into the database we avoid observing
          // changes on those ones and enabling/disabling call forwarding.
          if (_ignoreSettingChanges) {
            return;
          }
          // Bails out in case the reason is already enabled/disabled.
          if (_cfReasonStates[_cfReasonMapping[key]] === event.settingValue) {
            return;
          }
          var selector = 'input[data-setting="ril.cf.' + key + '.number"]';
          var textInput = document.querySelector(selector);
          var mozMobileCFInfo = {};

          mozMobileCFInfo['action'] = event.settingValue ?
            _cfAction.CALL_FORWARD_ACTION_REGISTRATION :
            _cfAction.CALL_FORWARD_ACTION_DISABLE;
          mozMobileCFInfo['reason'] = _cfReasonMapping[key];
          mozMobileCFInfo['serviceClass'] = _voiceServiceClassMask;

          // Skip the phone number checking when disabling call forwarding.
          if (event.settingValue && !cs_isPhoneNumberValid(textInput.value)) {
            showToast('callForwardingInvalidNumberError');

            cs_updateCallForwardingSubpanels();
            return;
          }
          mozMobileCFInfo['number'] = textInput.value;
          mozMobileCFInfo['timeSeconds'] =
            mozMobileCFInfo['reason'] !=
              _cfReason.CALL_FORWARD_REASON_NO_REPLY ? 0 : 20;

          var req = _mobileConnection.setCallForwardingOption(mozMobileCFInfo);

          cs_enableTapOnCallForwardingItems(false);
          cs_displayInfoForAll('callSettingsQuery');

          req.onsuccess = function() {
            cs_updateCallForwardingSubpanels(null,
                                             true,
                                             key,
                                             mozMobileCFInfo['action']);
          };
          req.onerror = function() {
            showToast('callForwardingSetError');
            cs_updateCallForwardingSubpanels();
          };
        });
      });
    }

    /**
     * Get the l10nId to show after setting up call forwarding.
     */
    function cs_getSetCallForwardingOptionResult(rules, action) {
      var l10nId;
      for (var i = 0; i < rules.length; i++) {
        if (rules[i].active &&
            ((_voiceServiceClassMask & rules[i].serviceClass) != 0)) {
          var disableAction = action === _cfAction.CALL_FORWARD_ACTION_DISABLE;
          l10nId = disableAction ?
            'callForwardingSetForbidden' : 'callForwardingSetSuccess';
          return l10nId;
        }
      }
      var registrationAction =
        action === _cfAction.CALL_FORWARD_ACTION_REGISTRATION;
      l10nId = registrationAction ?
        'callForwardingSetError' : 'callForwardingSetSuccess';
      return l10nId;
    }

    /**
     * Update call forwarding related subpanels.
     */
    function cs_updateCallForwardingSubpanels(callback,
                                              checkSetCallForwardingOptionResult,
                                              reason,
                                              action) {
      var element = document.getElementById('list-callForwarding');
      if (!element || element.hidden) {
        if (typeof callback === 'function') {
          callback(null);
        }
        return;
      }

      cs_displayInfoForAll('callSettingsQuery');
      cs_enableTapOnCallForwardingItems(false);
      _taskScheduler.enqueue('CALL_FORWARDING', function(done) {
        cs_getCallForwardingOption(function got_cfOption(cfOptions) {
          if (cfOptions) {
            // Need to check wether we enabled/disabled forwarding calls
            // properly e.g. the carrier might not support disabling call
            // forwarding for some reasons such as phone is busy, unreachable,
            // etc.
            if (checkSetCallForwardingOptionResult) {
              var rules = cfOptions[reason];
              var messageL10nId =
                cs_getSetCallForwardingOptionResult(rules, action);
              showToast(messageL10nId);
            }
            cs_displayRule(cfOptions['unconditional'], 'cfu-desc');
            cs_displayRule(cfOptions['mobilebusy'], 'cfmb-desc');
            cs_displayRule(cfOptions['noreply'], 'cfnrep-desc');
            cs_displayRule(cfOptions['notreachable'], 'cfnrea-desc');
            _getCallForwardingOptionSuccess = true;
            //  If the query is a success enable call forwarding items.
            cs_enableTapOnCallForwardingItems(_getCallForwardingOptionSuccess);
          } else {
            cs_displayInfoForAll('callSettingsQueryError');
            _getCallForwardingOptionSuccess = false;
            //  If the query is an error disable call forwarding items.
            cs_enableTapOnCallForwardingItems(_getCallForwardingOptionSuccess);
          }
          if (callback) {
            callback(null);
          }
          done();
        });
      });
    }

    /**
     *
     */
    function cs_enableTapOnCallerIdItem(enable) {
      var element = document.getElementById('menuItem-callerId');
      var select = element.querySelector('select');

      select.disabled = !enable;
      if (enable) {
        element.removeAttribute('aria-disabled');
      } else {
        element.setAttribute('aria-disabled', true);
      }
    }

    function cs_updateCallerIdPreference(callback) {
      _taskScheduler.enqueue('CALLER_ID_PREF', function(done) {
        if (typeof callback !== 'function') {
          callback = function() {
            done();
          };
        } else {
          var originalCallback = callback;
          callback = function() {
            originalCallback();
            done();
          };
        }

        cs_enableTapOnCallerIdItem(false);

        var req = _mobileConnection.getCallingLineIdRestriction();
        req.onsuccess = function() {
          var value = 0; //CLIR_DEFAULT

          // In some legitimates error cases (FdnCheckFailure), the req.result
          // is undefined. This is fine, we want this, and in this case we will
          // just display an error message for all the matching requests.
          if (req.result) {
            switch (req.result['m']) {
              case 1: // Permanently provisioned
              case 3: // Temporary presentation disallowed
              case 4: // Temporary presentation allowed
                switch (req.result['n']) {
                  case 1: // CLIR invoked, CLIR_INVOCATION
                  case 2: // CLIR suppressed, CLIR_SUPPRESSION
                  case 0: // Network default, CLIR_DEFAULT
                    value = req.result['n']; //'CLIR_INVOCATION'
                    break;
                  default:
                    value = 0; //CLIR_DEFAULT
                    break;
                }
                cs_enableTapOnCallerIdItem(true);
                break;
              case 0: // Not Provisioned
              case 2: // Unknown (network error, etc)
              default:
                value = 0; //CLIR_DEFAULT
                cs_enableTapOnCallerIdItem(false);
                break;
            }

            // Set the Call ID status,
            //   first item value for SIM1 and second item value for SIM2
            SettingsCache.getSettings(function(results) {
              var preferences = results['ril.clirMode'] || [0, 0];
              var targetIndex = DsdsSettings.getIccCardIndexForCallSettings();
              preferences[targetIndex] = value;
              var setReq = _settings.createLock().set({
                'ril.clirMode': preferences
              });
              setReq.onsuccess = callback;
              setReq.onerror = callback;
            });
          } else {
            callback();
          }
        };
        req.onerror = callback;
      });
    }

    /**
     *
     */
    function cs_updateCallerIdItemState(callback) {
      var element = document.getElementById('menuItem-callerId');
      if (!element || element.hidden) {
        if (typeof callback === 'function') {
          callback(null);
        }
        return;
      }

      _taskScheduler.enqueue('CALLER_ID', function(done) {
        SettingsCache.getSettings(function(results) {
          var targetIndex = DsdsSettings.getIccCardIndexForCallSettings();
          var preferences = results['ril.clirMode'];
          var preference = (preferences && preferences[targetIndex]) || 0;
          var input = document.getElementById('ril-callerId');

          var value;
          switch (preference) {
            case 1: // CLIR invoked
              value = 'CLIR_INVOCATION';
              break;
            case 2: // CLIR suppressed
              value = 'CLIR_SUPPRESSION';
              break;
            case 0: // Network default
            default:
              value = 'CLIR_DEFAULT';
              break;
          }

          input.value = value;

          if (typeof callback === 'function') {
            callback();
          }
          done();
        });
      });
    }

    function cs_checkCallerId(clirMode) {
      var targetIndex = DsdsSettings.getIccCardIndexForCallSettings();
      var getReq = _settings.createLock().get('ril.clirMode');
      getReq.onsuccess = function () {
        var preferences = getReq.result['ril.clirMode'];
        var preference = (preferences && preferences[targetIndex]) || 0;
        if (clirMode === preference) {
          showToast('changessaved');
        }
      };
    }

    /**
     *
     */
    function cs_initCallerId() {
      var element = document.getElementById('ril-callerId');

      var updateItem = function() {
        cs_updateCallerIdItemState(function() {
          cs_enableTapOnCallerIdItem(true);
        });
      };

      var updatePreferenceAndItem =
        cs_updateCallerIdPreference.bind(null, updateItem);

      // We listen for change events so that way we set the CLIR mode once the
      // user change the option value.
      element.addEventListener('change', (event) => {
        var clirMode = _clirConstantsMapping[element.value];
        var setReq = _mobileConnection.setCallingLineIdRestriction(clirMode);
        // If the setting success, system app will sync the value.
        // If the setting fails, we force sync the value here and update the UI.
        setReq.onerror = updatePreferenceAndItem;
        setReq.onsuccess = () => {
          cs_updateCallerIdPreference();
          cs_checkCallerId(clirMode);
        }
      });
    }

    /**
     * Enable/Disable call waiting settings page
     */
    function cs_enableTapOnCallWaitingItem(enable) {
      var menuItem = document.getElementById('menuItem-callWaiting');
      var select = menuItem.querySelector('select');
      var descText = menuItem.querySelector('small');
      // update call waiting query status
      _callWaitingQueryStatus = enable;

      // update the description
      function getSelectValue() {
        var enabled = select.value === 'true';
        var status = '';
        if (select) {
          status = enabled ? 'enabled' : 'disabled';
        }
        return status;
      }

      if (descText && !enable) {
        descText.setAttribute('data-l10n-id', 'callSettingsQuery');
      } else {
        // Clear the data-l10n-id information
        descText.innerHTML = '';
        descText.setAttribute('data-l10n-id', getSelectValue());
      }

      if (enable) {
        menuItem.removeAttribute('aria-disabled');
      } else {
        menuItem.setAttribute('aria-disabled', true);
      }
    }

    /**
     *
     */
    function cs_updateCallWaitingItemState(callback) {
      var menuItem = document.getElementById('menuItem-callWaiting');
      if (!menuItem || menuItem.hidden) {
        if (typeof callback === 'function') {
          callback(null);
        }
        return;
      }

      cs_enableTapOnCallWaitingItem(false);

      _taskScheduler.enqueue('CALL_WAITING', function(done) {
        var select = menuItem.querySelector('select');

        var getCWEnabled = _mobileConnection.getCallWaitingOption();
        getCWEnabled.onsuccess = function cs_getCWEnabledSuccess() {
          var enabled = getCWEnabled.result;
          select.value = enabled;
          menuItem.dataset.state = enabled ? 'on' : 'off';
          cs_enableTapOnCallWaitingItem(true);

          if (callback) {
            callback(null);
          }
          done();
        };
        getCWEnabled.onerror = function cs_getCWEnabledError() {
          menuItem.dataset.state = 'unknown';
          if (callback) {
            callback(null);
          }
          done();
        };
      });
    }

    function cs_enableTapOnCallBarringItem(enable) {
      var element = document.getElementById('menuItem-callBarring');
      var hrefItem = element.querySelector('a');
      if (enable) {
        element.removeAttribute('aria-disabled');
        hrefItem.setAttribute('data-href', '#call-cbSettings');
      } else {
        // @HACK To make user can't enter any page
        // when the devices get the SIM card state.
        hrefItem.removeAttribute('data-href');
        element.setAttribute('aria-disabled', true);
      }
    }

    /**
     * @HACK To Enable the "Call Waiting" select option
     */
    function _enableCallWaitingSelect(evt) {
      if (evt.key === 'Enter') {
        var select = document.querySelector('li.focus select');
        if (select && _callWaitingQueryStatus) {
          select.hidden = false;
          select.focus();
        }
        select.hidden = true;
      }
    }

    /**
     *
     */
    function cs_initCallWaiting() {
      var callWaitingItem = document.getElementById('menuItem-callWaiting');
      callWaitingItem.addEventListener('keydown', _enableCallWaitingSelect);

      // Bind call waiting setting to the input
      var select =
        document.querySelector('#menuItem-callWaiting select');
      select.addEventListener('change', function cs_cwInputChanged(event) {
        var handleSetCallWaiting = function cs_handleSetCallWaiting() {
          cs_updateCallWaitingItemState(function() {
            cs_enableTapOnCallWaitingItem(true);
          });
        };
        cs_enableTapOnCallWaitingItem(false);
        var enabled = (select.value === 'true') || false;
        var req = _mobileConnection.setCallWaitingOption(enabled);
        req.onerror = handleSetCallWaiting;
        req.onsuccess = () => {
          cs_updateCallWaitingItemState(function() {
            cs_enableTapOnCallWaitingItem(true);
            showToast('changessaved');
          });
        };
      });
    }

    /**
     *
     */
    function cs_updateVoiceMailItemState() {
      var voiceMailMenuItem = document.getElementById('voiceMail-desc');
      var targetIndex = DsdsSettings.getIccCardIndexForCallSettings();

      voiceMailMenuItem.textContent = '';
      SettingsCache.getSettings(function(results) {
        var numbers = results['ril.iccInfo.mbdn'];
        var number = numbers[targetIndex];
        if (number) {
          voiceMailMenuItem.removeAttribute('data-l10n-id');
          voiceMailMenuItem.textContent = number;
        } else {
          voiceMailMenuItem.setAttribute('data-l10n-id',
                                         'voiceMail-number-notSet');
        }
      });
    }

    function cs_initVoiceMailClickEvent() {
      document.querySelector('.menuItem-voicemail').onclick = function() {
        DialogService.show('call-voiceMailSettings');
      };
    }

    /**
     *
     */
    function cs_initVoiceMailSettings() {
      // update all voice numbers if necessary
      SettingsCache.getSettings(function(results) {
        var settings = navigator.mozSettings;
        var voicemail = navigator.mozVoicemail;
        var updateVMNumber = false;
        var numbers = results['ril.iccInfo.mbdn'] || [];

        Array.prototype.forEach.call(_mobileConnections, function(conn, index) {
          var number = numbers[index];
          // If the voicemail number has not been stored into the database yet
          // we check whether the number is provided by the mozVoicemail API. In
          // that case we store it into the setting database.
          if (!number && voicemail) {
            number = voicemail.getNumber(index);
            if (number) {
              updateVMNumber = true;
              numbers[index] = number;
            }
          }
        });

        if (updateVMNumber) {
          var req = settings.createLock().set({
            'ril.iccInfo.mbdn': numbers
          });
          req.onsuccess = function() {
            cs_updateVoiceMailItemState();
            settings.addObserver('ril.iccInfo.mbdn', function() {
              cs_updateVoiceMailItemState();
            });
          };
        } else {
          cs_updateVoiceMailItemState();
          settings.addObserver('ril.iccInfo.mbdn', function() {
            cs_updateVoiceMailItemState();
          });
        }
      });
    }

    function cs_updateVoicePrivacyItemState() {
      var menuItem = document.getElementById('menuItem-voicePrivacyMode');
      if (!menuItem || menuItem.hidden) {
        return;
      }

      var privacyModeSelect = menuItem.querySelector('select');
      var getReq = _mobileConnection.getVoicePrivacyMode();
      getReq.onsuccess = function get_vpm_success() {
        privacyModeSelect.value = getReq.result;
      };
      getReq.onerror = function get_vpm_error() {
        console.warn('get voice privacy mode: ' + getReq.error.name);
      };
    }

    /**
     * Init voice privacy mode.
     */
    function cs_initVoicePrivacyMode() {
      var defaultVoicePrivacySettings =
      Array.prototype.map.call(_mobileConnections,
        function() { return [true, true]; });
      var voicePrivacyHelper =
        SettingsHelper('ril.voicePrivacy.enabled', defaultVoicePrivacySettings);
      var privacyModeItem =
        document.getElementById('menuItem-voicePrivacyMode');
      var privacyModeSelect =
        privacyModeItem.querySelector('select');

      privacyModeSelect.addEventListener('change',
        function vpm_inputChanged() {
          var checked = (privacyModeSelect.value === 'true' || false);
          voicePrivacyHelper.get(function gotVP(values) {
            var originalValue = !checked;
            var setReq = _mobileConnection.setVoicePrivacyMode(checked);
            setReq.onsuccess = function set_vpm_success() {
              var targetIndex = DsdsSettings.getIccCardIndexForCallSettings();
              values[targetIndex] = !originalValue;
              voicePrivacyHelper.set(values);
            };
            setReq.onerror = function get_vpm_error() {
              // restore the value if failed.
              privacyModeSelect.value = originalValue;
            };
          });
      });
    }

    /**
     *
     */
    function cs_updateFdnStatus() {
      var iccObj = getIccByIndex();
      if (!iccObj) {
        return;
      }

      var req = iccObj.getCardLock('fdn');
      req.onsuccess = function spl_checkSuccess() {
        var enabled = req.result.enabled;

        var simFdnDesc = document.querySelector('#fdnSettings-desc');
        simFdnDesc.setAttribute('data-l10n-id', enabled ? 'on' : 'off');

        var fdnSettingsBlocked = document.querySelector('#fdnSettingsBlocked');
        if (fdnSettingsBlocked) {
          fdnSettingsBlocked.hidden = !enabled;
        }
        var callForwardingItem =
          document.getElementById('menuItem-callForwarding');
        callForwardingItem.hidden = enabled;
        window.dispatchEvent(new CustomEvent('refresh'));
      };
    }
    return {
      init:  function(panel) {
        cs_init();
      },
      onBeforeShow: function() {
        cs_addNetworkTypeCheckListener();
      },
      onBeforeHide: function() {
        cs_removeNetworkTypeCheckListener();
      }
    };
  };
  return CallSettings;
});

/**
 * TaskScheduler helps manage tasks and ensures they are executed in
 * sequential order. When a task of a certain type is enqueued, all pending
 * tasks of the same type in the queue are removed. This avoids redundant
 * queries and improves user perceived performance.
 */
var TaskScheduler = function() {
  return {
    _isLocked: false,
    _tasks: [],
    _lock: function() {
      this._isLocked = true;
    },
    _unlock: function() {
      this._isLocked = false;
      this._executeNextTask();
    },
    _removeRedundantTasks: function(type) {
      return this._tasks.filter(function(task) {
        return task.type !== type;
      });
    },
    _executeNextTask: function() {
      if (this._isLocked) {
        return;
      }
      var nextTask = this._tasks.shift();
      if (nextTask) {
        this._lock();
        nextTask.func(function() {
          this._unlock();
        }.bind(this));
      }
    },
    enqueue: function(type, func) {
      this._tasks = this._removeRedundantTasks(type);
      this._tasks.push({
        type: type,
        func: func
      });
      this._executeNextTask();
    }
  };
};
