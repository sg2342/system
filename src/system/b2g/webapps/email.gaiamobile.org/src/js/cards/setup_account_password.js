define([ "require", "mix", "cards", "form_navigation", "./base", "template!./setup_account_password.html", "./setup_account_error_mixin" ], function(e) {
    function t(e) {
        return new r({
            formElem: e.formNode,
            onLast: e.onNext.bind(e)
        });
    }
    var n = e("mix"), o = e("cards"), r = e("form_navigation");
    return [ e("./base")(e("template!./setup_account_password.html")), e("./setup_account_error_mixin"), {
        onArgs: function(e) {
            this.args = e, this.emailAddress = e.emailAddress, this.emailNode.textContent = this.emailAddress, 
            this.needsFocus = !0, t(this);
        },
        onCardVisible: function() {
            this.needsFocus && (this.passwordNode.focus(), this.needsFocus = !1);
        },
        onBack: function() {
            o.removeCardAndSuccessors(this, "animate", 1);
        },
        onNext: function(e) {
            e.preventDefault(), this.args.password = this.passwordNode.value, o.pushCard("setup_progress", "animate", n({
                callingCard: this
            }, this.args), "right");
        },
        onInfoInput: function() {
            this.nextButton.disabled = !this.formNode.checkValidity();
        },
        die: function() {}
    } ];
});