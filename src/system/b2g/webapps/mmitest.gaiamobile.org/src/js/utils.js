/* © 2017 KAI OS TECHNOLOGIES (HONG KONG) LIMITED All rights reserved.
 * This file or any portion thereof may not be reproduced or used in any manner
 * whatsoever without the express written permission of KAI OS TECHNOLOGIES
 * (HONG KONG) LIMITED. KaiOS is the trademark of KAI OS TECHNOLOGIES (HONG KONG)
 * LIMITED or its affiliate company and may be registered in some jurisdictions.
 * All other trademarks are the property of their respective owners.
 */
// ************************************************************************
// * File Name: utils.js
// * Description: To provide the public tools
// * Note:
// ************************************************************************

const DEBUG = true;

function debug(s) {
  if (DEBUG) {
    dump(' -- [mmitest] -- [utils.js] = ' + s + '\n');
  } else {
    // TODO: If we set the DEBUG value to false, we should save the log into a local file.
  }
}

function getMenuList() {
  let xhr = new XMLHttpRequest();
  return new Promise((resolve, reject) => {
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          let configPara = JSON.parse(xhr.responseText);
          _removeItems(configPara.testItems);
          resolve(configPara.testItems);
        } else {
          debug('Get the config.json file failed status: ' + xhr.status);
          reject(null);
        }
      }
    };

    xhr.overrideMimeType('application/json');
    xhr.open('GET', '/resource/config.json', true);
    xhr.send(null);
  });
}

// Remove the device unsupported test item
function _removeItems(list) {
  let i = 0,
  len = list.length;
  for (i; i < len; i++) {
    /*switch (list[i].htmlId) {
      case 'nfc':
        // Hiding the NFC testing if device not support it.
        if (!navigator.mozNfc) {
          debug('Device don\'t support NFC.');
          list.splice(i, 1);
          len -= 1;//Defect351 -tao.li@t2mobile.com-MMITest can not load.
        }
        break;
      case 'camera_front':
        let cameraAmount = navigator.mozCameras.getListOfCameras().length;
        if (2 !== cameraAmount) {
          list.splice(i, 1);
          //Defect351 -tao.li@t2mobile.com-MMITest can not load.-begin
          debug('Device don\'t support front camera.');
          len -= 1;
          //Defect351 -tao.li@t2mobile.com-MMITest can not load.-end
        }
        break;*/
    let item = list[i];
    if (item) {
      switch (item.htmlId) {
        case 'nfc':
          // Hiding the NFC testing if device not support it.
          if (!navigator.mozNfc) {
            debug('Device don\'t support NFC.');
            list.splice(i, 1);
            len -= 1;
          }
          break;
        case 'camera_front':
          let cameraAmount = navigator.mozCameras.getListOfCameras().length;
          if (2 !== cameraAmount) {
            debug('Device don\'t have front camera.');
            list.splice(i, 1);
            len -= 1;
          }
          break;
      }
    }
  }
}
