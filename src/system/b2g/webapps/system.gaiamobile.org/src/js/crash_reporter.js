/* -*- Mode: js; js-indent-level: 2; indent-tabs-mode: nil -*- */
/* vim: set shiftwidth=2 tabstop=2 autoindent cindent expandtab: */

'use strict';

// This file calls getElementById without waiting for an onload event, so it
// must have a defer attribute or be included at the end of the <body>.

var CrashReporter = (function() {
  var _ = navigator.mozL10n.get;
  var settings = navigator.mozSettings;
  var screen = document.getElementById('screen');
  var softkeyparams = null;

  // The name of the app that just crashed. We'll have special handling for
  // when this remains null or is set to null.
  var crashedAppName = null;

  // Whether or not to show a "Report" button in the banner.
  var showReportButton = false;

  // Only show the "Report" button if the user hasn't set a preference to
  // always/never report crashes.
  SettingsListener.observe('app.reportCrashes', 'ask',
      function handleCrashSetting(value) {
    showReportButton = (value != 'always' && value != 'never');
  });

  // This function should only ever be called once.
  function showDialog(crashID, isChrome) {
    var elem = document.getElementById('crash-dialog-title');
    if (isChrome) {
      navigator.mozL10n.setAttributes(elem, 'crash-dialog-os2');
    } else {
      navigator.mozL10n.setAttributes(
        elem,
        'crash-dialog-app',
        { name: crashedAppName || _('crash-dialog-app-noname') }
      );
    }
      //add softbar
    var params = {
      menuClassName: 'menu-button',
      header: { l10nId:'' },
      items: [
      {
        name: '',
        l10nId: '',
        priority: 1,
        method: function() {
          SoftkeyHelper.hide();
          settings.createLock().set({'app.reportCrashes': 'never'});
          removeDialog();
        }
      },
      {
        name: 'Send Report',
        l10nId: 'tcl-send-report',
        priority: 3,
        method: function() {
          SoftkeyHelper.hide();
          submitCrash(crashID);
          if (checkbox.checked) {
            settings.createLock().set({'app.reportCrashes': 'always'});
          }
          removeDialog();
        }
      },
      ]
    };
    SoftkeyHelper.init(params,function(){
      SoftkeyHelper.hide();
      removeDialog();
    });



    softkeyparams = params; 
    var checkbox = document.getElementById('always-send');
    checkbox.addEventListener('click', function onCheckboxClick() {
      // Disable the "Don't Send Report" button if the "Always send..."
      // checkbox is checked
      if(this.checked){
        SoftkeyHelper.updateSoftkey(softkeyparams,'1','');
      }else{
        SoftkeyHelper.updateSoftkey(softkeyparams,'1','tcl-not-send');
      }
    });
    // "What's in a crash report?" link
    var crashInfoLink = document.getElementById('crash-info-link');
    crashInfoLink.addEventListener('click', function onLearnMoreClick() {
      var dialog = document.getElementById('crash-dialog');
      //add softkey
    var params = {
      menuClassName: 'menu-button',
      header: { l10nId:'' },
      items: [
      {
        name: 'Cancel',
        l10nId: 'tcl-cancelButtonId',
        priority: 1,
        method: function() {
          SoftkeyHelper.hide();
          console.log("berton remove learn-more");
          dialog.classList.remove('learn-more');
          SoftkeyHelper.init(softkeyparams,function(){
          settings.createLock().set({'app.reportCrashes': 'never'});
          removeDialog();
      });
        }
      }
      ]
     };
      SoftkeyHelper.init(params,function(){
      SoftkeyHelper.hide();
      dialog.classList.remove('learn-more');
      SoftkeyHelper.init(softkeyparams,function(){
      SoftkeyHelper.hide();
      removeDialog();
     });
     });
      dialog.classList.add('learn-more');
    });


    screen.classList.add('crash-dialog');
  }
  function handleEndKey() {
    SoftkeyHelper.hide();
    removeDialog();
  }

  // We can get rid of the dialog after it is shown once.
  function removeDialog() {
    screen.classList.remove('crash-dialog');
    var dialog = document.getElementById('crash-dialog');
    dialog.parentNode.removeChild(dialog);
    Service.request('focus');
    window.removeEventListener('end-key-press', handleEndKey);
  }

  function showBanner(crashID, isChrome) {
    var appName = crashedAppName || _('crash-dialog-app-noname');
    var message = isChrome ? _('crash-banner-os2') :
      _('crash-banner-app', { name: appName });

    Service.request('SystemToaster:show', {
      title: message
    });
  }

  function deleteCrash(crashID) {
    var event = document.createEvent('CustomEvent');
    event.initCustomEvent('mozContentEvent', true, true, {
      type: 'delete-crash',
      crashID: crashID
    });
    window.dispatchEvent(event);
  }

  function submitCrash(crashID) {
    var event = document.createEvent('CustomEvent');
    event.initCustomEvent('mozContentEvent', true, true, {
      type: 'submit-crash',
      crashID: crashID
    });
    window.dispatchEvent(event);
  }

  // - Show a dialog only the first time there's a crash to report.
  // - On subsequent crashes, show a banner letting the user know there was a
  //   crash.
  // - If the user hasn't set a pref, add a "Report" button to the banner.
  function handleCrash(crashID, isChrome) {
    showBanner(crashID, isChrome);
  }

  // We depend on window_manager.js calling this function before
  // we get a 'handle-crash' event from shell.js
  function setAppName(name) {
    crashedAppName = name;
  }

  // We will be notified of system crashes from shell.js
  window.addEventListener('mozChromeEvent', function handleChromeEvent(e) {
    if (e.detail.type == 'handle-crash') {
      handleCrash(e.detail.crashID, e.detail.chrome);
    }
  });

  function handleAppCrash(e) {
    var app = e.detail;
    // Only show crash reporter when the crashed app is active.
    if (app.isActive()) {
      setAppName(app.name);
    }
  }

  function handlekey(e) {
   switch(e.key){
    case 'MozHomeScreen':
      SoftkeyHelper.hide();
      removeDialog();
      break;
  }
   }
  window.addEventListener('appcrashed', handleAppCrash);
  window.addEventListener('activitycrashed', handleAppCrash);
  window.addEventListener('homescreencrashed', handleAppCrash);
  window.addEventListener('searchcrashed', handleAppCrash);
  window.addEventListener('keydown', handlekey);
  window.addEventListener('end-key-press', handleEndKey);
  return {
    handleCrash: handleCrash,
    handleAppCrash: handleAppCrash,
    setAppName: setAppName
  };
})();

