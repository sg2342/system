"use strict";let videoElement=document.getElementsByTagName("video")[0];document.addEventListener("keypress",ev=>{if(ev.synthetic) 
return; if(ev.key=="F11"&&videoElement.videoWidth!=0&&videoElement.videoHeight!=0){
if(window.fullScreen){return;}
ev.preventDefault();ev.stopPropagation();if(!document.fullscreenElement){videoElement.requestFullscreen();}else{document.exitFullscreen();}
return;}

if(document.activeElement==videoElement)
return;let newEvent=new KeyboardEvent("keypress",ev);newEvent.synthetic=true;videoElement.dispatchEvent(newEvent);});