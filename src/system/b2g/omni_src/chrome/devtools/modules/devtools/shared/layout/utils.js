"use strict";const{Ci,Cc}=require("chrome");const{memoize}=require("sdk/lang/functional");loader.lazyRequireGetter(this,"setIgnoreLayoutChanges","devtools/server/actors/layout",true);exports.setIgnoreLayoutChanges=(...args)=>this.setIgnoreLayoutChanges(...args);const utilsFor=memoize(win=>win.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindowUtils));function getTopWindow(win){let docShell=win.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIWebNavigation).QueryInterface(Ci.nsIDocShell);if(!docShell.isMozBrowserOrApp){return win.top;}
let topDocShell=docShell.getSameTypeRootTreeItemIgnoreBrowserAndAppBoundaries();return topDocShell?topDocShell.contentViewer.DOMDocument.defaultView:null;}
exports.getTopWindow=getTopWindow;const isTopWindow=win=>win&&getTopWindow(win)===win;exports.isTopWindow=isTopWindow;function isWindowIncluded(boundaryWindow,win){if(win===boundaryWindow){return true;}
let parent=getParentWindow(win);if(!parent||parent===win){return false;}
return isWindowIncluded(boundaryWindow,parent);}
exports.isWindowIncluded=isWindowIncluded;function getParentWindow(win){if(isTopWindow(win)){return null;}
let docShell=win.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIWebNavigation).QueryInterface(Ci.nsIDocShell);if(!docShell.isMozBrowserOrApp){return win.parent;}
let parentDocShell=docShell.getSameTypeParentIgnoreBrowserAndAppBoundaries();return parentDocShell?parentDocShell.contentViewer.DOMDocument.defaultView:null;}
exports.getParentWindow=getParentWindow;const getFrameElement=(win)=>isTopWindow(win)?null:utilsFor(win).containerElement;exports.getFrameElement=getFrameElement;function getFrameOffsets(boundaryWindow,node){let xOffset=0;let yOffset=0;let frameWin=node.ownerDocument.defaultView;let scale=getCurrentZoom(node);if(boundaryWindow===null){boundaryWindow=getTopWindow(frameWin);}else if(typeof boundaryWindow==="undefined"){throw new Error("No boundaryWindow given. Use null for the default one.");}
while(frameWin!==boundaryWindow){let frameElement=getFrameElement(frameWin);if(!frameElement){break;}

let frameRect=frameElement.getBoundingClientRect();let[offsetTop,offsetLeft]=getFrameContentOffset(frameElement);xOffset+=frameRect.left+offsetLeft;yOffset+=frameRect.top+offsetTop;frameWin=getParentWindow(frameWin);}
return[xOffset*scale,yOffset*scale];}
function getAdjustedQuads(boundaryWindow,node,region){if(!node||!node.getBoxQuads){return[];}
let quads=node.getBoxQuads({box:region});if(!quads.length){return[];}
let[xOffset,yOffset]=getFrameOffsets(boundaryWindow,node);let scale=getCurrentZoom(node);let adjustedQuads=[];for(let quad of quads){adjustedQuads.push({p1:{w:quad.p1.w*scale,x:quad.p1.x*scale+xOffset,y:quad.p1.y*scale+yOffset,z:quad.p1.z*scale},p2:{w:quad.p2.w*scale,x:quad.p2.x*scale+xOffset,y:quad.p2.y*scale+yOffset,z:quad.p2.z*scale},p3:{w:quad.p3.w*scale,x:quad.p3.x*scale+xOffset,y:quad.p3.y*scale+yOffset,z:quad.p3.z*scale},p4:{w:quad.p4.w*scale,x:quad.p4.x*scale+xOffset,y:quad.p4.y*scale+yOffset,z:quad.p4.z*scale},bounds:{bottom:quad.bounds.bottom*scale+yOffset,height:quad.bounds.height*scale,left:quad.bounds.left*scale+xOffset,right:quad.bounds.right*scale+xOffset,top:quad.bounds.top*scale+yOffset,width:quad.bounds.width*scale,x:quad.bounds.x*scale+xOffset,y:quad.bounds.y*scale+yOffset}});}
return adjustedQuads;}
exports.getAdjustedQuads=getAdjustedQuads;function getRect(boundaryWindow,node,contentWindow){let frameWin=node.ownerDocument.defaultView;let clientRect=node.getBoundingClientRect();if(boundaryWindow===null){boundaryWindow=getTopWindow(frameWin);}else if(typeof boundaryWindow==="undefined"){throw new Error("No boundaryWindow given. Use null for the default one.");}
let rect={top:clientRect.top+contentWindow.pageYOffset,left:clientRect.left+contentWindow.pageXOffset,width:clientRect.width,height:clientRect.height};while(frameWin!==boundaryWindow){let frameElement=getFrameElement(frameWin);if(!frameElement){break;}

let frameRect=frameElement.getBoundingClientRect();let[offsetTop,offsetLeft]=getFrameContentOffset(frameElement);rect.top+=frameRect.top+offsetTop;rect.left+=frameRect.left+offsetLeft;frameWin=getParentWindow(frameWin);}
return rect;}
exports.getRect=getRect;function getNodeBounds(boundaryWindow,node){if(!node){return null;}
let scale=getCurrentZoom(node); let offsetLeft=0;let offsetTop=0;let el=node;while(el&&el.parentNode){offsetLeft+=el.offsetLeft;offsetTop+=el.offsetTop;el=el.offsetParent;} 
el=node;while(el&&el.parentNode){if(el.scrollTop){offsetTop-=el.scrollTop;}
if(el.scrollLeft){offsetLeft-=el.scrollLeft;}
el=el.parentNode;} 
let[xOffset,yOffset]=getFrameOffsets(boundaryWindow,node);xOffset+=offsetLeft;yOffset+=offsetTop;xOffset*=scale;yOffset*=scale; let width=node.offsetWidth*scale;let height=node.offsetHeight*scale;return{p1:{x:xOffset,y:yOffset},p2:{x:xOffset+width,y:yOffset},p3:{x:xOffset+width,y:yOffset+height},p4:{x:xOffset,y:yOffset+height}};}
exports.getNodeBounds=getNodeBounds;function safelyGetContentWindow(frame){if(frame.contentWindow){return frame.contentWindow;}
let walker=Cc["@mozilla.org/inspector/deep-tree-walker;1"].createInstance(Ci.inIDeepTreeWalker);walker.showSubDocuments=true;walker.showDocumentsAsNodes=true;walker.init(frame,Ci.nsIDOMNodeFilter.SHOW_ALL);walker.currentNode=frame;let document=walker.nextNode();if(!document||!document.defaultView){throw new Error("Couldn't get the content window inside frame "+frame);}
return document.defaultView;}
function getFrameContentOffset(frame){let style=safelyGetContentWindow(frame).getComputedStyle(frame,null); if(!style){return[0,0];}
let paddingTop=parseInt(style.getPropertyValue("padding-top"),10);let paddingLeft=parseInt(style.getPropertyValue("padding-left"),10);let borderTop=parseInt(style.getPropertyValue("border-top-width"),10);let borderLeft=parseInt(style.getPropertyValue("border-left-width"),10);return[borderTop+paddingTop,borderLeft+paddingLeft];}
function getElementFromPoint(document,x,y){let node=document.elementFromPoint(x,y);if(node&&node.contentDocument){if(node instanceof Ci.nsIDOMHTMLIFrameElement){let rect=node.getBoundingClientRect();let[offsetTop,offsetLeft]=getFrameContentOffset(node);x-=rect.left+offsetLeft;y-=rect.top+offsetTop;if(x<0||y<0){return node;}}
if(node instanceof Ci.nsIDOMHTMLIFrameElement||node instanceof Ci.nsIDOMHTMLFrameElement){let subnode=getElementFromPoint(node.contentDocument,x,y);if(subnode){node=subnode;}}}
return node;}
exports.getElementFromPoint=getElementFromPoint;function scrollIntoViewIfNeeded(elem,centered=true){let win=elem.ownerDocument.defaultView;let clientRect=elem.getBoundingClientRect();
let topToBottom=clientRect.bottom;let bottomToTop=clientRect.top-win.innerHeight;let yAllowed=true;
if((topToBottom>0||!centered)&&topToBottom<=elem.offsetHeight){win.scrollBy(0,topToBottom-elem.offsetHeight);yAllowed=false;}else if((bottomToTop<0||!centered)&&bottomToTop>=-elem.offsetHeight){win.scrollBy(0,bottomToTop+elem.offsetHeight);yAllowed=false;}
if(centered){if(yAllowed&&(topToBottom<=0||bottomToTop>=0)){win.scroll(win.scrollX,win.scrollY+clientRect.top
-(win.innerHeight-elem.offsetHeight)/2);}}}
exports.scrollIntoViewIfNeeded=scrollIntoViewIfNeeded;function isNodeConnected(node){if(!node.ownerDocument||!node.ownerDocument.defaultView){return false;}
try{return!(node.compareDocumentPosition(node.ownerDocument.documentElement)&node.DOCUMENT_POSITION_DISCONNECTED);}catch(e){ return false;}}
exports.isNodeConnected=isNodeConnected;function getRootBindingParent(node){let parent;let doc=node.ownerDocument;if(!doc){return node;}
while((parent=doc.getBindingParent(node))){node=parent;}
return node;}
exports.getRootBindingParent=getRootBindingParent;function getBindingParent(node){let doc=node.ownerDocument;if(!doc){return null;}
let parent=doc.getBindingParent(node);if(!parent){return null;}
return parent;}
exports.getBindingParent=getBindingParent;const isAnonymous=(node)=>getRootBindingParent(node)!==node;exports.isAnonymous=isAnonymous;const hasBindingParent=(node)=>!!getBindingParent(node);const isNativeAnonymous=(node)=>hasBindingParent(node)&&!(isXBLAnonymous(node)||isShadowAnonymous(node));exports.isNativeAnonymous=isNativeAnonymous;function isXBLAnonymous(node){let parent=getBindingParent(node);if(!parent){return false;}
if(parent.shadowRoot&&parent.shadowRoot.contains(node)){return false;}
let anonNodes=[...node.ownerDocument.getAnonymousNodes(parent)||[]];return anonNodes.indexOf(node)>-1;}
exports.isXBLAnonymous=isXBLAnonymous;function isShadowAnonymous(node){let parent=getBindingParent(node);if(!parent){return false;}
 
return parent.shadowRoot&&parent.shadowRoot.contains(node);}
exports.isShadowAnonymous=isShadowAnonymous;function getCurrentZoom(node){let win=null;if(node instanceof Ci.nsIDOMNode){win=node.ownerDocument.defaultView;}else if(node instanceof Ci.nsIDOMWindow){win=node;}
if(!win){throw new Error("Unable to get the zoom from the given argument.");}
return utilsFor(win).fullZoom;}
exports.getCurrentZoom=getCurrentZoom;