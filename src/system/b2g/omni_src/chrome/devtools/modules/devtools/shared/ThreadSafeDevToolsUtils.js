"use strict";exports.immutableUpdate=function(...objs){return Object.freeze(Object.assign({},...objs));};exports.update=function update(target,...args){for(let attrs of args){for(let key in attrs){let desc=Object.getOwnPropertyDescriptor(attrs,key);if(desc){Object.defineProperty(target,key,desc);}}}
return target;};exports.values=function values(object){return Object.keys(object).map(k=>object[k]);};exports.reportException=function reportException(who,exception){const msg=`${who} threw an exception: ${exports.safeErrorString(exception)}`;dump(msg+"\n");if(typeof console!=="undefined"&&console&&console.error){console.error(msg);}};exports.makeInfallible=function(handler,name=handler.name){return function(){try{return handler.apply(this,arguments);}catch(ex){let who="Handler function";if(name){who+=" "+name;}
exports.reportException(who,ex);return undefined;}};};exports.safeErrorString=function(error){try{let errorString=error.toString();if(typeof errorString=="string"){
try{if(error.stack){let stack=error.stack.toString();if(typeof stack=="string"){errorString+="\nStack: "+stack;}}}catch(ee){}
if(typeof error.lineNumber=="number"&&typeof error.columnNumber=="number"){errorString+="Line: "+error.lineNumber+", column: "+error.columnNumber;}
return errorString;}}catch(ee){}
return Object.prototype.toString.call(error);};exports.zip=function(a,b){if(!b){return a;}
if(!a){return b;}
const pairs=[];for(let i=0,aLength=a.length,bLength=b.length;i<aLength||i<bLength;i++){pairs.push([a[i],b[i]]);}
return pairs;};exports.entries=function entries(obj){return Object.keys(obj).map(k=>[k,obj[k]]);};exports.toObject=function(arr){const obj={};for(let[k,v]of arr){obj[k]=v;}
return obj;};exports.compose=function compose(...funcs){return(...args)=>{const initialValue=funcs[funcs.length-1](...args);const leftFuncs=funcs.slice(0,-1);return leftFuncs.reduceRight((composed,f)=>f(composed),initialValue);};};exports.isGenerator=function(fn){if(typeof fn!=="function"){return false;}
let proto=Object.getPrototypeOf(fn);if(!proto){return false;}
let ctor=proto.constructor;if(!ctor){return false;}
return ctor.name=="GeneratorFunction";};exports.isPromise=function(p){return p&&typeof p.then==="function";};exports.isSavedFrame=function(thing){return Object.prototype.toString.call(thing)==="[object SavedFrame]";};exports.isSet=function(thing){return Object.prototype.toString.call(thing)==="[object Set]";};exports.flatten=function(lists){return Array.prototype.concat.apply([],lists);};