"use strict";const{Cu}=require("chrome");const protocol=require("devtools/server/protocol");const{custom,method,RetVal,Arg,Option,types,preEvent}=protocol;const{actorBridge}=require("devtools/server/actors/common");loader.lazyRequireGetter(this,"events","sdk/event/core");loader.lazyRequireGetter(this,"merge","sdk/util/object",true);loader.lazyRequireGetter(this,"PerformanceIO","devtools/client/performance/modules/io");loader.lazyRequireGetter(this,"RecordingUtils","devtools/shared/performance/recording-utils");loader.lazyRequireGetter(this,"PerformanceRecordingCommon","devtools/shared/performance/recording-common",true);var PerformanceRecordingActor=exports.PerformanceRecordingActor=protocol.ActorClass(merge({typeName:"performance-recording",form:function(detail){if(detail==="actorid"){return this.actorID;}
let form={actor:this.actorID, configuration:this._configuration,startingBufferStatus:this._startingBufferStatus,console:this._console,label:this._label,startTime:this._startTime,localStartTime:this._localStartTime,recording:this._recording,completed:this._completed,duration:this._duration,};
 if(this._profile&&!this._sentFinalizedData){form.finalizedData=true;form.profile=this.getProfile();form.systemHost=this.getHostSystemInfo();form.systemClient=this.getClientSystemInfo();this._sentFinalizedData=true;}
return form;},initialize:function(conn,options,meta){protocol.Actor.prototype.initialize.call(this,conn);this._configuration={withMarkers:options.withMarkers||false,withTicks:options.withTicks||false,withMemory:options.withMemory||false,withAllocations:options.withAllocations||false,allocationsSampleProbability:options.allocationsSampleProbability||0,allocationsMaxLogLength:options.allocationsMaxLogLength||0,bufferSize:options.bufferSize||0,sampleFrequency:options.sampleFrequency||1};this._console=!!options.console;this._label=options.label||"";if(meta){


 this._localStartTime=Date.now();this._startTime=meta.startTime;this._startingBufferStatus={position:meta.position,totalSize:meta.totalSize,generation:meta.generation};this._recording=true;this._markers=[];this._frames=[];this._memory=[];this._ticks=[];this._allocations={sites:[],timestamps:[],frames:[],sizes:[]};this._systemHost=meta.systemHost||{};this._systemClient=meta.systemClient||{};}},destroy:function(){protocol.Actor.prototype.destroy.call(this);},_setState:function(state,extraData){switch(state){case"recording-started":{this._recording=true;break;}
case"recording-stopping":{this._recording=false;break;}
case"recording-stopped":{this._profile=extraData.profile;this._duration=extraData.duration;


RecordingUtils.offsetSampleTimes(this._profile,this._startTime);
this._markers=this._markers.sort((a,b)=>(a.start>b.start));this._completed=true;break;}};},},PerformanceRecordingCommon));var PerformanceRecordingFront=exports.PerformanceRecordingFront=protocol.FrontClass(PerformanceRecordingActor,merge({form:function(form,detail){if(detail==="actorid"){this.actorID=form;return;}
this.actorID=form.actor;this._form=form;this._configuration=form.configuration;this._startingBufferStatus=form.startingBufferStatus;this._console=form.console;this._label=form.label;this._startTime=form.startTime;this._localStartTime=form.localStartTime;this._recording=form.recording;this._completed=form.completed;this._duration=form.duration;if(form.finalizedData){this._profile=form.profile;this._systemHost=form.systemHost;this._systemClient=form.systemClient;}


if(this._completed&&!this._markersSorted){this._markers=this._markers.sort((a,b)=>(a.start>b.start));this._markersSorted=true;}},initialize:function(client,form,config){protocol.Front.prototype.initialize.call(this,client,form);this._markers=[];this._frames=[];this._memory=[];this._ticks=[];this._allocations={sites:[],timestamps:[],frames:[],sizes:[]};},destroy:function(){protocol.Front.prototype.destroy.call(this);},exportRecording:function(file){let recordingData=this.getAllData();return PerformanceIO.saveRecordingToFile(recordingData,file);},_addTimelineData:function(eventName,data){let config=this.getConfiguration();switch(eventName){
case"markers":{if(!config.withMarkers){break;}
let{markers}=data;RecordingUtils.offsetMarkerTimes(markers,this._startTime);RecordingUtils.pushAll(this._markers,markers);break;}
case"frames":{if(!config.withMarkers){break;}
let{frames}=data;RecordingUtils.pushAll(this._frames,frames);break;}

case"memory":{if(!config.withMemory){break;}
let{delta,measurement}=data;this._memory.push({delta:delta-this._startTime,value:measurement.total/1024/1024});break;}
case"ticks":{if(!config.withTicks){break;}
let{timestamps}=data;this._ticks=timestamps;break;}
case"allocations":{if(!config.withAllocations){break;}
let{allocations:sites,allocationsTimestamps:timestamps,allocationSizes:sizes,frames,}=data;RecordingUtils.offsetAndScaleTimestamps(timestamps,this._startTime);RecordingUtils.pushAll(this._allocations.sites,sites);RecordingUtils.pushAll(this._allocations.timestamps,timestamps);RecordingUtils.pushAll(this._allocations.frames,frames);RecordingUtils.pushAll(this._allocations.sizes,sizes);break;}}},toString:()=>"[object PerformanceRecordingFront]"},PerformanceRecordingCommon));