"use strict";const{classes:Cc,interfaces:Ci,utils:Cu}=Components;Cu.import("resource://gre/modules/XPCOMUtils.jsm");Cu.import("resource://gre/modules/Promise.jsm");Cu.import("resource://gre/modules/Task.jsm");Cu.import("resource://gre/modules/systemlibs.js");Cu.import("resource://gre/modules/Services.jsm");XPCOMUtils.defineLazyServiceGetter(this,"UiccConnector","@mozilla.org/secureelement/connector/uicc;1","nsISecureElementConnector");XPCOMUtils.defineLazyServiceGetter(this,"gSettingsService","@mozilla.org/settingsService;1","nsISettingsService");XPCOMUtils.defineLazyModuleGetter(this,"SEUtils","resource://gre/modules/SEUtils.jsm");XPCOMUtils.defineLazyGetter(this,"SE",function(){let obj={};Cu.import("resource://gre/modules/se_consts.js",obj);return obj;});XPCOMUtils.defineLazyGetter(this,"GP",function(){let obj={};Cu.import("resource://gre/modules/gp_consts.js",obj);return obj;});XPCOMUtils.defineLazyGetter(this,"UiccConnector",()=>{let uiccClass=Cc["@mozilla.org/secureelement/connector/uicc;1"];return uiccClass?uiccClass.getService(Ci.nsISecureElementConnector):null;});XPCOMUtils.defineLazyGetter(this,"EseConnector",()=>{let eseClass=Cc["@mozilla.org/secureelement/connector/ese;1"];return eseClass?eseClass.getService(Ci.nsISecureElementConnector):null;});XPCOMUtils.defineLazyGetter(this,"NFC",function(){let obj={};Cu.import("resource://gre/modules/nfc_consts.js",obj);return obj;});







const APDU_STATUS_LEN=2;const GET_ALL_DATA_COMMAND_LENGTH=2;const RESPONSE_STATUS_LENGTH=2;const GET_REFRESH_DATA_CMD_LEN=2;const GET_REFRESH_DATA_TAG_LEN=1;const GET_REFRESH_TAG_LEN=8;const REF_AR_DO=0xE2;const REF_DO=0xE1;const AR_DO=0xE3;const HASH_REF_DO=0xC1;const AID_REF_DO=0x4F;const APDU_AR_DO=0xD0;const NFC_AR_DO=0xD1;var DEBUG=SE.DEBUG_ACE;var debug;function updateDebug(){if(DEBUG){debug=function(s){dump("ACE:GPAccessRulesManager: "+s+"\n");};}else{debug=function(s){};}};updateDebug();function getConnector(type){switch(type){case SE.TYPE_UICC:return UiccConnector;case SE.TYPE_ESE:return EseConnector;default:debug("Unsupported SEConnector : "+type);return null;}}
function GPAccessRulesManager(){let lock=gSettingsService.createLock();lock.get(NFC.SETTING_NFC_DEBUG,this);Services.obs.addObserver(this,NFC.TOPIC_MOZSETTINGS_CHANGED,false);}
GPAccessRulesManager.prototype={ PKCS_AID:"a000000063504b43532d3135",ARAM_AID:"A00000015141434C00",
 READ_BINARY:[GP.CLA_SM,GP.INS_RB,GP.P1_RB,GP.P2_RB],GET_RESPONSE:[GP.CLA_SM,GP.INS_GR,GP.P1_GR,GP.P2_GR],SELECT_BY_DF:[GP.CLA_SM,GP.INS_SF,GP.P1_SF_DF,GP.P2_SF_FCP], channel:null,REFRESH_TAG_PATH:[GP.TAG_SEQUENCE,GP.TAG_OCTETSTRING],refreshTag:null, rules:[],seType:SE.TYPE_ESE,getAccessRules:function getAccessRules(seType){debug("getAccessRules: seType:"+seType);this.seType=seType;return new Promise((resolve,reject)=>{if(seType==SE.TYPE_ESE){this._readARAMAccessRules(()=>resolve(this.rules));}else{this._readAccessRules(()=>resolve(this.rules));}});},_updateAccessRules:Task.async(function*(done){try{yield this._openChannel(this.ARAM_AID);yield this._updateAllGPData();yield this._closeChannel();done();}catch(error){debug("_updateAccessRules: "+error);yield this._closeChannel();done();}}),_updateAllGPData:function _updateAllGPData(){let apdu=this._bytesToAPDU(GP.UPDATE_ALL_GP_DATA);return new Promise((resolve,reject)=>{getConnector(this.seType).exchangeAPDU(this.channel,apdu.cla,apdu.ins,apdu.p1,apdu.p2,apdu.data,apdu.le,{notifyExchangeAPDUResponse:(sw1,sw2,data)=>{debug("Update all data command response is "+sw1.toString(16)+sw2.toString(16)+" data: "+data+" length:"+(data.length/2));if(sw1!==0x90&&sw2!==0x00){debug("rejecting APDU response");reject(new Error("Response "+sw1+","+sw2));return;}
resolve();},notifyError:(error)=>{debug("_exchangeAPDU/notifyError "+error);reject(error);}});});},_readARAMAccessRules:Task.async(function*(done){try{yield this._openChannel(this.ARAM_AID);if(this.rules){let refreshTag=yield this._readRefreshGPData();debug("New refresh tag is "+refreshTag+" old refresh tag is "+this.refreshTag);if(SEUtils.arraysEqual(this.refreshTag,refreshTag)){debug("Refresh tag equals to the one saved.");yield this._closeChannel();return done();}
this.refreshTag=refreshTag;}
this.rules=yield this._readAllGPData();DEBUG&&debug("_readAccessRules: "+JSON.stringify(this.rules).toString(16));yield this._closeChannel();done();}catch(error){debug("_readARAMAccessRules: "+error);this.rules=[];yield this._closeChannel();done();}}),_getRulesLength:function _getRulesLength(apdu,rulesLength){if(apdu.length<3)
return-1;let length;length=apdu[2];rulesLength.numBytes=1;
if(length&0x80){let _length=length&0x7f;debug("Length:"+_length);if(apdu.length<3+_length)
return-1;length=0;let base
base=1<<8*_length;for(let i=0;i<_length;i++){base>>=8;length+=apdu[3+i]*base;}
rulesLength.numBytes=_length+1;}
rulesLength.length=length;},_parseArDo:function _parseArDo(bytes){let result={};result[APDU_AR_DO]=[];result[NFC_AR_DO]=[];debug("Parse AR_DO:")
for(let pos=0;pos<bytes.length;){let tag=bytes[pos],length=bytes[pos+1],parsed=null;switch(tag){case APDU_AR_DO:let apduRuleValue=bytes.slice(pos+2,pos+2+length);debug("apdu rule:"+apduRuleValue);result[APDU_AR_DO]=apduRuleValue;break;case NFC_AR_DO:let nfcRuleValue=bytes.slice(pos+2,pos+2+length);debug("nfc event rule:"+nfcRuleValue);result[NFC_AR_DO]=nfcRuleValue;break;default:break;}
pos=pos+2+length;}
return result;},_parseRefDo:function _parseRefDo(bytes){let result={};result[HASH_REF_DO]=[];result[AID_REF_DO]=[];debug("Parse REF_DO:")
for(let pos=0;pos<bytes.length;){let tag=bytes[pos],length=bytes[pos+1],parsed=null;switch(tag){case HASH_REF_DO:let hashRefValue=bytes.slice(pos+2,pos+2+length);debug("application hash:"+hashRefValue);result[HASH_REF_DO]=hashRefValue;break;case AID_REF_DO:let aidRefValue=bytes.slice(pos+2,pos+2+length);debug("aid hash:"+aidRefValue);result[AID_REF_DO]=aidRefValue;break;default:break;}
pos=pos+2+length;}
return result;},_parseARRule:function _parseARRule(rule){let result={};result[REF_DO]=[];result[AR_DO]=[];debug("Parse Rule:")
for(let pos=0;pos<rule.length;){let tag=rule[pos],length=rule[pos+1],value=rule.slice(pos+2,pos+2+length),parsed=null;switch(tag){case REF_DO:result[REF_DO]=this._parseRefDo(value);break;case AR_DO:result[AR_DO]=this._parseArDo(value);break;default:break;}
pos=pos+2+length;}
return{hash:result[REF_DO][HASH_REF_DO],aid:result[REF_DO][AID_REF_DO],apduRules:result[AR_DO][APDU_AR_DO],nfcRule:result[AR_DO][NFC_AR_DO]}},_parseARRules:function _parseARRules(payload){let rules=[];let containerTags=[REF_DO,AR_DO,];debug("Parse Rules:")
let rule;for(let pos=0;pos<payload.length;){if(payload[pos]!=REF_AR_DO){pos++;continue;}
let refArDoLength=payload[pos+1];let refArDo=payload.slice(pos+2,pos+2+refArDoLength);rule=this._parseARRule(refArDo);debug("rule:"+JSON.stringify(rule));pos+=2+refArDoLength;rules.push(rule);}
return rules;},_parseResponse:function _parseResponse(apdu){let rulesLength={numBytes:0,length:0};this._getRulesLength(apdu,rulesLength);debug("Total rules length: "+rulesLength.length+" number of bytes to represent length:"+
rulesLength.numBytes);if(rulesLength.length<0){debug("Invalid total rules length");return[];}
if(apdu.length>GET_ALL_DATA_COMMAND_LENGTH+rulesLength.numBytes+
rulesLength.length+RESPONSE_STATUS_LENGTH){debug("Invalid apdu length");return[];}
let payloadLength=apdu.length-GET_ALL_DATA_COMMAND_LENGTH-
rulesLength.numBytes-RESPONSE_STATUS_LENGTH;if(payloadLength<rulesLength.length){ debug("Should send get next data command, received data length "+
payloadLength+" expected length "+rulesLength.length);let headerLength=GET_ALL_DATA_COMMAND_LENGTH+rulesLength.numBytes;let payload=apdu.slice(headerLength,headerLength+payloadLength);return this._readNextGPData(payload,rulesLength.length);}
let headerLength=GET_ALL_DATA_COMMAND_LENGTH+rulesLength.numBytes;let payload=apdu.slice(headerLength,headerLength+payloadLength);return this._parseARRules(payload);},_readNextGPData:function _readNextGPData(prevPayload,totalRulesLength){let apdu=this._bytesToAPDU(GP.GET_NEXT_GP_DATA);return new Promise((resolve,reject)=>{getConnector(this.seType).exchangeAPDU(this.channel,apdu.cla,apdu.ins,apdu.p1,apdu.p2,apdu.data,apdu.le,{notifyExchangeAPDUResponse:(sw1,sw2,data)=>{debug("Read next data command response is "+sw1.toString(16)+sw2.toString(16)+" data: "+data+" length:"+(data.length/2));if(sw1!==0x90&&sw2!==0x00){debug("rejecting APDU response");reject(new Error("Response "+sw1+","+sw2));return;}
let raw=SEUtils.hexStringToByteArray(data);let resp=raw.slice(0,(raw.length-2));let accumulated=prevPayload.concat(resp);if(accumulated.length<totalRulesLength){debug("Get next data command current length:"+accumulated.length+" expected length:"+totalRulesLength);this._readNextGPData(accumulated,totalRulesLength);}else{debug("Get all access rules");let rules=this._parseARRules(accumulated);resolve(rules);}},notifyError:(error)=>{debug("_exchangeAPDU/notifyError "+error);reject(error);}});});},_readAllGPData:function _readAllGPData(){let apdu=this._bytesToAPDU(GP.GET_ALL_GP_DATA);return new Promise((resolve,reject)=>{getConnector(this.seType).exchangeAPDU(this.channel,apdu.cla,apdu.ins,apdu.p1,apdu.p2,apdu.data,apdu.le,{notifyExchangeAPDUResponse:(sw1,sw2,data)=>{debug("Read all data command response is "+sw1.toString(16)+sw2.toString(16)+" data: "+data+" length:"+(data.length/2));if(sw1!==0x90&&sw2!==0x00){debug("rejecting APDU response");reject(new Error("Response "+sw1+","+sw2));return;}
let resp=SEUtils.hexStringToByteArray(data);let rules=this._parseResponse(resp);resolve(rules);},notifyError:(error)=>{debug("_exchangeAPDU/notifyError "+error);reject(error);}});});},_readRefreshGPData:function _readRefreshGPData(){let apdu=this._bytesToAPDU(GP.GET_REFRESH_GP_DATA);return new Promise((resolve,reject)=>{getConnector(this.seType).exchangeAPDU(this.channel,apdu.cla,apdu.ins,apdu.p1,apdu.p2,apdu.data,apdu.le,{notifyExchangeAPDUResponse:(sw1,sw2,data)=>{debug("Read refresh tag command response is "+sw1.toString(16)+sw2.toString(16)+" data: "+data);if(sw1!==0x90&&sw2!==0x00){debug("rejecting APDU response");reject(new Error("Response "+sw1+","+sw2));return;}
let resp=SEUtils.hexStringToByteArray(data);if(resp.length!=(GET_REFRESH_DATA_CMD_LEN+GET_REFRESH_DATA_TAG_LEN+
GET_REFRESH_TAG_LEN+APDU_STATUS_LEN)){reject(new Error("Incorrect data length"));return;}
if(resp[0]!=0xDF||resp[1]!=0x20||resp[2]!=0x08){reject(new Error("Invalid refersh tag"));return;}
let refreshTag=resp.slice(GET_REFRESH_DATA_CMD_LEN+
GET_REFRESH_DATA_TAG_LEN,GET_REFRESH_DATA_CMD_LEN+
GET_REFRESH_DATA_TAG_LEN+
GET_REFRESH_TAG_LEN);resolve(refreshTag);},notifyError:(error)=>{debug("_exchangeAPDU/notifyError "+error);reject(error);}});});},_readAccessRules:Task.async(function*(done){try{yield this._openChannel(this.PKCS_AID);let odf=yield this._readODF();let dodf=yield this._readDODF(odf);let acmf=yield this._readACMF(dodf);let refreshTag=acmf[this.REFRESH_TAG_PATH[0]]
[this.REFRESH_TAG_PATH[1]];if(SEUtils.arraysEqual(this.refreshTag,refreshTag)){debug("_readAccessRules: refresh tag equals to the one saved.");yield this._closeChannel();return done();}
this.refreshTag=refreshTag;debug("_readAccessRules: refresh tag saved: "+this.refreshTag);let acrf=yield this._readACRules(acmf);let accf=yield this._readACConditions(acrf);this.rules=yield this._parseRules(acrf,accf);DEBUG&&debug("_readAccessRules: "+JSON.stringify(this.rules,0,2));yield this._closeChannel();done();}catch(error){debug("_readAccessRules: "+error);this.rules=[];yield this._closeChannel();done();}}),_openChannel:function _openChannel(aid){if(this.channel!==null){debug("_openChannel: Channel already opened, rejecting.");return Promise.reject();}
return new Promise((resolve,reject)=>{getConnector(this.seType).openChannel(aid,{notifyOpenChannelSuccess:(channel,openResponse)=>{debug("_openChannel/notifyOpenChannelSuccess: Channel "+channel+" opened, open response: "+openResponse);this.channel=channel;resolve();},notifyError:(error)=>{debug("_openChannel/notifyError: failed to open channel, error: "+
error);reject(error);}});});},_closeChannel:function _closeChannel(){if(this.channel===null){debug("_closeChannel: Channel not opened, rejecting.");return Promise.reject();}
return new Promise((resolve,reject)=>{getConnector(this.seType).closeChannel(this.channel,{notifyCloseChannelSuccess:()=>{debug("_closeChannel/notifyCloseChannelSuccess: chanel "+
this.channel+" closed");this.channel=null;resolve();},notifyError:(error)=>{debug("_closeChannel/notifyError: error closing channel, error"+
error);reject(error);}});});},_exchangeAPDU:function _exchangeAPDU(bytes){DEBUG&&debug("apdu "+JSON.stringify(bytes));let apdu=this._bytesToAPDU(bytes);return new Promise((resolve,reject)=>{getConnector(this.seType).exchangeAPDU(this.channel,apdu.cla,apdu.ins,apdu.p1,apdu.p2,apdu.data,apdu.le,{notifyExchangeAPDUResponse:(sw1,sw2,data)=>{debug("APDU response is "+sw1.toString(16)+sw2.toString(16)+" data: "+data);if(sw1!==0x90&&sw2!==0x00){debug("rejecting APDU response");reject(new Error("Response "+sw1+","+sw2));return;}
resolve(this._parseTLV(data));},notifyError:(error)=>{debug("_exchangeAPDU/notifyError "+error);reject(error);}});});},_readBinaryFile:function _readBinaryFile(selectResponse){DEBUG&&debug("Select response: "+JSON.stringify(selectResponse));
let fileLength=selectResponse[GP.TAG_FCP][0x80];if(fileLength[0]===0&&fileLength[1]===0){return Promise.resolve(null);}

return this._exchangeAPDU(this.READ_BINARY);},_selectAndRead:function _selectAndRead(df){return this._exchangeAPDU(this.SELECT_BY_DF.concat(df.length&0xFF,df)).then((resp)=>this._readBinaryFile(resp));},_readODF:function _readODF(){debug("_readODF");return this._selectAndRead(GP.ODF_DF);},_readDODF:function _readDODF(odfFile){debug("_readDODF, ODF file: "+odfFile);



let DODF_DF=odfFile[GP.TAG_EF_ODF][GP.TAG_SEQUENCE][GP.TAG_OCTETSTRING];return this._selectAndRead(DODF_DF);},_readACMF:function _readACMF(dodfFile){debug("_readACMF, DODF file: "+dodfFile);



















 
let gpdOid=[0x2A, 0x86,0x48, 0x86,0xFC,0x6B, 0x81,0x48, 0x01, 0x01]; let records=SEUtils.ensureIsArray(dodfFile[GP.TAG_EXTERNALDO]);let gpdRecords=records.filter((record)=>{let oid=record[GP.TAG_EXTERNALDO][GP.TAG_SEQUENCE][GP.TAG_OID];return SEUtils.arraysEqual(oid,gpdOid);});

if(gpdRecords.length!==1){return Promise.reject(new Error(gpdRecords.length+" ACMF files found"));}
let ACMain_DF=gpdRecords[0][GP.TAG_EXTERNALDO][GP.TAG_SEQUENCE]
[GP.TAG_SEQUENCE][GP.TAG_OCTETSTRING];return this._selectAndRead(ACMain_DF);},_readACRules:function _readACRules(acMainFile){debug("_readACRules, ACMain file: "+acMainFile);




let ACRules_DF=acMainFile[GP.TAG_SEQUENCE][GP.TAG_SEQUENCE][GP.TAG_OCTETSTRING];return this._selectAndRead(ACRules_DF);},_readACConditions:function _readACConditions(acRulesFile){debug("_readACCondition, ACRules file: "+acRulesFile);let acRules=SEUtils.ensureIsArray(acRulesFile[GP.TAG_SEQUENCE]);if(acRules.length===0){debug("No rules found in ACRules file.");return Promise.reject(new Error("No rules found in ACRules file"));}

let acReadQueue=Promise.resolve({});acRules.forEach((ruleEntry)=>{let df=ruleEntry[GP.TAG_SEQUENCE][GP.TAG_OCTETSTRING];let readAcCondition=(acConditionFiles)=>{if(acConditionFiles[df]!==undefined){debug("Skipping previously read acCondition df: "+df);return acConditionFiles;}
return this._selectAndRead(df).then((acConditionFileContents)=>{acConditionFiles[df]=acConditionFileContents;return acConditionFiles;});}
acReadQueue=acReadQueue.then(readAcCondition);});return acReadQueue;},_parseRules:function _parseRules(acRulesFile,acConditionFiles){DEBUG&&debug("_parseRules: acConditionFiles "+JSON.stringify(acConditionFiles));let rules=[];let acRules=SEUtils.ensureIsArray(acRulesFile[GP.TAG_SEQUENCE]);acRules.forEach((ruleEntry)=>{DEBUG&&debug("Parsing one rule: "+JSON.stringify(ruleEntry));let rule={};

let oneApplet=ruleEntry[GP.TAG_GPD_AID];let allApplets=ruleEntry[GP.TAG_GPD_ALL];if(oneApplet){rule.applet=oneApplet[GP.TAG_OCTETSTRING];}else if(allApplets){rule.applet=Ci.nsIAccessRulesManager.ALL_APPLET;}else{throw Error("Unknown applet definition");}
let df=ruleEntry[GP.TAG_SEQUENCE][GP.TAG_OCTETSTRING];let condition=acConditionFiles[df];if(condition===null){rule.application=Ci.nsIAccessRulesManager.DENY_ALL;}else if(condition[GP.TAG_SEQUENCE]){if(!Array.isArray(condition[GP.TAG_SEQUENCE])&&!condition[GP.TAG_SEQUENCE][GP.TAG_OCTETSTRING]){rule.application=Ci.nsIAccessRulesManager.ALLOW_ALL;}else{rule.application=SEUtils.ensureIsArray(condition[GP.TAG_SEQUENCE]).map((conditionEntry)=>{return conditionEntry[GP.TAG_OCTETSTRING];});}}else{throw Error("Unknown application definition");}
DEBUG&&debug("Rule parsed, adding to the list: "+JSON.stringify(rule));rules.push(rule);});DEBUG&&debug("All rules parsed, we have those in total: "+JSON.stringify(rules));return rules;},_parseTLV:function _parseTLV(bytes){let containerTags=[GP.TAG_SEQUENCE,GP.TAG_FCP,GP.TAG_GPD_AID,GP.TAG_EXTERNALDO,GP.TAG_INDIRECT,GP.TAG_EF_ODF];return SEUtils.parseTLV(bytes,containerTags);},
 _bytesToAPDU:function _bytesToAPDU(arr){let apdu;if(arr.length>4){apdu={cla:arr[0]&0xFF,ins:arr[1]&0xFF,p1:arr[2]&0xFF,p2:arr[3]&0xFF,le:-1};let data=arr.slice(4);apdu.data=(data.length)?SEUtils.byteArrayToHexString(data):"";}else{apdu={cla:arr[0]&0xFF,ins:arr[1]&0xFF,p1:arr[2]&0xFF,p2:arr[3]&0xFF,data:"",le:-1};}
return apdu;},handle:function handle(name,result){switch(name){case NFC.SETTING_NFC_DEBUG:DEBUG=result;updateDebug();break;}},observe:function observe(subject,topic,data){switch(topic){case NFC.TOPIC_MOZSETTINGS_CHANGED:if("wrappedJSObject"in subject){subject=subject.wrappedJSObject;}
if(subject){this.handle(subject.key,subject.value);}
break;}},classID:Components.ID("{3e046b4b-9e66-439a-97e0-98a69f39f55f}"),contractID:"@mozilla.org/secureelement/access-control/rules-manager;1",QueryInterface:XPCOMUtils.generateQI([Ci.nsIAccessRulesManager])};this.NSGetFactory=XPCOMUtils.generateNSGetFactory([GPAccessRulesManager]);