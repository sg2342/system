"use strict";const Cc=Components.classes;const Ci=Components.interfaces;const Cr=Components.results;const Cu=Components.utils;Cu.import("resource://gre/modules/XPCOMUtils.jsm");Cu.import("resource://gre/modules/AddonManager.jsm");Cu.import("resource://gre/modules/Services.jsm");Cu.import("resource://gre/modules/Preferences.jsm");XPCOMUtils.defineLazyModuleGetter(this,"PromptUtils","resource://gre/modules/SharedPromptUtils.jsm");const URI_XPINSTALL_DIALOG="chrome://mozapps/content/xpinstall/xpinstallConfirm.xul";const READY_STATES=[AddonManager.STATE_AVAILABLE,AddonManager.STATE_DOWNLOAD_FAILED,AddonManager.STATE_INSTALL_FAILED,AddonManager.STATE_CANCELLED];Cu.import("resource://gre/modules/Log.jsm");const LOGGER_ID="addons.weblistener";
var logger=Log.repository.getLogger(LOGGER_ID);function notifyObservers(aTopic,aBrowser,aUri,aInstalls){let info={browser:aBrowser,originatingURI:aUri,installs:aInstalls,QueryInterface:XPCOMUtils.generateQI([Ci.amIWebInstallInfo])};Services.obs.notifyObservers(info,aTopic,null);}
function Installer(aBrowser,aUrl,aInstalls){this.browser=aBrowser;this.url=aUrl;this.downloads=aInstalls;this.installed=[];notifyObservers("addon-install-started",aBrowser,aUrl,aInstalls);for(let install of aInstalls){install.addListener(this); if(READY_STATES.indexOf(install.state)!=-1)
install.install();}
this.checkAllDownloaded();}
Installer.prototype={browser:null,downloads:null,installed:null,isDownloading:true,checkAllDownloaded:function(){
if(!this.isDownloading)
return;var failed=[];var installs=[];for(let install of this.downloads){switch(install.state){case AddonManager.STATE_AVAILABLE:case AddonManager.STATE_DOWNLOADING:
 return;case AddonManager.STATE_DOWNLOAD_FAILED:failed.push(install);break;case AddonManager.STATE_DOWNLOADED: if(install.addon.appDisabled)
failed.push(install);else
installs.push(install);if(install.linkedInstalls){for(let linkedInstall of install.linkedInstalls){linkedInstall.addListener(this); if(linkedInstall.state==AddonManager.STATE_DOWNLOAD_FAILED||linkedInstall.addon.appDisabled)
failed.push(linkedInstall);else
installs.push(linkedInstall);}}
break;case AddonManager.STATE_CANCELLED: break;default:logger.warn("Download of "+install.sourceURI.spec+" in unexpected state "+
install.state);}}
this.isDownloading=false;this.downloads=installs;if(failed.length>0){
for(let install of failed){if(install.state==AddonManager.STATE_DOWNLOADED){install.removeListener(this);install.cancel();}}
notifyObservers("addon-install-failed",this.browser,this.url,failed);} 
if(this.downloads.length==0)
return;
 if("@mozilla.org/addons/web-install-prompt;1"in Cc){try{let prompt=Cc["@mozilla.org/addons/web-install-prompt;1"].getService(Ci.amIWebInstallPrompt);prompt.confirm(this.browser,this.url,this.downloads,this.downloads.length);return;}
catch(e){}}
if(Preferences.get("xpinstall.customConfirmationUI",false)){notifyObservers("addon-install-confirmation",this.browser,this.url,this.downloads);return;}
let args={};args.url=this.url;args.installs=this.downloads;args.wrappedJSObject=args;try{Cc["@mozilla.org/base/telemetry;1"].getService(Ci.nsITelemetry).getHistogramById("SECURITY_UI").add(Ci.nsISecurityUITelemetry.WARNING_CONFIRM_ADDON_INSTALL);let parentWindow=null;if(this.browser){parentWindow=this.browser.ownerDocument.defaultView;PromptUtils.fireDialogEvent(parentWindow,"DOMWillOpenModalDialog",this.browser);}
Services.ww.openWindow(parentWindow,URI_XPINSTALL_DIALOG,null,"chrome,modal,centerscreen",args);}catch(e){logger.warn("Exception showing install confirmation dialog",e);for(let install of this.downloads){install.removeListener(this);
install.cancel();}
notifyObservers("addon-install-cancelled",this.browser,this.url,this.downloads);}},checkAllInstalled:function(){var failed=[];for(let install of this.downloads){switch(install.state){case AddonManager.STATE_DOWNLOADED:case AddonManager.STATE_INSTALLING:
 return;case AddonManager.STATE_INSTALL_FAILED:failed.push(install);break;}}
this.downloads=null;if(failed.length>0)
notifyObservers("addon-install-failed",this.browser,this.url,failed);if(this.installed.length>0)
notifyObservers("addon-install-complete",this.browser,this.url,this.installed);this.installed=null;},onDownloadCancelled:function(aInstall){aInstall.removeListener(this);this.checkAllDownloaded();},onDownloadFailed:function(aInstall){aInstall.removeListener(this);this.checkAllDownloaded();},onDownloadEnded:function(aInstall){this.checkAllDownloaded();return false;},onInstallCancelled:function(aInstall){aInstall.removeListener(this);this.checkAllInstalled();},onInstallFailed:function(aInstall){aInstall.removeListener(this);this.checkAllInstalled();},onInstallEnded:function(aInstall){aInstall.removeListener(this);this.installed.push(aInstall); if(aInstall.addon.type=="theme"&&aInstall.addon.userDisabled==true&&aInstall.addon.appDisabled==false){aInstall.addon.userDisabled=false;}
this.checkAllInstalled();}};function extWebInstallListener(){}
extWebInstallListener.prototype={onWebInstallDisabled:function(aBrowser,aUri,aInstalls){let info={browser:aBrowser,originatingURI:aUri,installs:aInstalls,QueryInterface:XPCOMUtils.generateQI([Ci.amIWebInstallInfo])};Services.obs.notifyObservers(info,"addon-install-disabled",null);},onWebInstallOriginBlocked:function(aBrowser,aUri,aInstalls){let info={browser:aBrowser,originatingURI:aUri,installs:aInstalls,install:function(){},QueryInterface:XPCOMUtils.generateQI([Ci.amIWebInstallInfo])};Services.obs.notifyObservers(info,"addon-install-origin-blocked",null);return false;},onWebInstallBlocked:function(aBrowser,aUri,aInstalls){let info={browser:aBrowser,originatingURI:aUri,installs:aInstalls,install:function(){new Installer(this.browser,this.originatingURI,this.installs);},QueryInterface:XPCOMUtils.generateQI([Ci.amIWebInstallInfo])};Services.obs.notifyObservers(info,"addon-install-blocked",null);return false;},onWebInstallRequested:function(aBrowser,aUri,aInstalls){new Installer(aBrowser,aUri,aInstalls); return false;},classDescription:"XPI Install Handler",contractID:"@mozilla.org/addons/web-install-listener;1",classID:Components.ID("{0f38e086-89a3-40a5-8ffc-9b694de1d04a}"),QueryInterface:XPCOMUtils.generateQI([Ci.amIWebInstallListener,Ci.amIWebInstallListener2])};this.NSGetFactory=XPCOMUtils.generateNSGetFactory([extWebInstallListener]);