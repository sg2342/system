const{utils:Cu,interfaces:Ci,classes:Cc,results:Cr}=Components;Cu.import("resource://gre/modules/Services.jsm");Cu.import("resource://gre/modules/AppConstants.jsm");Cu.import("resource://gre/modules/XPCOMUtils.jsm");XPCOMUtils.defineLazyModuleGetter(this,"EnableDelayHelper","resource://gre/modules/SharedPromptUtils.jsm"); function isUsableDirectory(aDirectory)
{return aDirectory.exists()&&aDirectory.isDirectory()&&aDirectory.isWritable();}

function nsUnknownContentTypeDialogProgressListener(aHelperAppDialog){this.helperAppDlg=aHelperAppDialog;}
nsUnknownContentTypeDialogProgressListener.prototype={onStatusChange:function(aWebProgress,aRequest,aStatus,aMessage){if(aStatus!=Components.results.NS_OK){Services.prompt.alert(this.dialog,this.helperAppDlg.mTitle,aMessage);this.helperAppDlg.onCancel();if(this.helperAppDlg.mDialog){this.helperAppDlg.mDialog.close();}}},onProgressChange:function(aWebProgress,aRequest,aCurSelfProgress,aMaxSelfProgress,aCurTotalProgress,aMaxTotalProgress){},onProgressChange64:function(aWebProgress,aRequest,aCurSelfProgress,aMaxSelfProgress,aCurTotalProgress,aMaxTotalProgress){},onStateChange:function(aWebProgress,aRequest,aStateFlags,aStatus){},onLocationChange:function(aWebProgress,aRequest,aLocation,aFlags){},onSecurityChange:function(aWebProgress,aRequest,state){},onRefreshAttempted:function(aWebProgress,aURI,aDelay,aSameURI){return true;}}; const PREF_BD_USEDOWNLOADDIR="browser.download.useDownloadDir";const nsITimer=Components.interfaces.nsITimer;var downloadModule={};Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");Components.utils.import("resource://gre/modules/DownloadLastDir.jsm",downloadModule);Components.utils.import("resource://gre/modules/DownloadPaths.jsm");Components.utils.import("resource://gre/modules/DownloadUtils.jsm");Components.utils.import("resource://gre/modules/Downloads.jsm");Components.utils.import("resource://gre/modules/FileUtils.jsm");Components.utils.import("resource://gre/modules/Task.jsm");function nsUnknownContentTypeDialog(){this.mLauncher=null;this.mContext=null;this.mReason=null;this.chosenApp=null;this.givenDefaultApp=false;this.updateSelf=true;this.mTitle="";}
nsUnknownContentTypeDialog.prototype={classID:Components.ID("{F68578EB-6EC2-4169-AE19-8C6243F0ABE1}"),nsIMIMEInfo:Components.interfaces.nsIMIMEInfo,QueryInterface:function(iid){if(!iid.equals(Components.interfaces.nsIHelperAppLauncherDialog)&&!iid.equals(Components.interfaces.nsITimerCallback)&&!iid.equals(Components.interfaces.nsISupports)){throw Components.results.NS_ERROR_NO_INTERFACE;}
return this;},

show:function(aLauncher,aContext,aReason){this.mLauncher=aLauncher;this.mContext=aContext;this.mReason=aReason;try{let parent=aContext.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindow);this._mDownloadDir=new downloadModule.DownloadLastDir(parent);}catch(ex){Cu.reportError("Missing window information when showing nsIHelperAppLauncherDialog: "+ex);}
const nsITimer=Components.interfaces.nsITimer;this._showTimer=Components.classes["@mozilla.org/timer;1"].createInstance(nsITimer);this._showTimer.initWithCallback(this,0,nsITimer.TYPE_ONE_SHOT);},

reallyShow:function(){try{var ir=this.mContext.QueryInterface(Components.interfaces.nsIInterfaceRequestor);var dwi=ir.getInterface(Components.interfaces.nsIDOMWindow);var ww=Components.classes["@mozilla.org/embedcomp/window-watcher;1"].getService(Components.interfaces.nsIWindowWatcher);this.mDialog=ww.openWindow(dwi,"chrome://mozapps/content/downloads/unknownContentType.xul",null,"chrome,centerscreen,titlebar,dialog=yes,dependent",null);}catch(ex){
this.mLauncher.cancel(Components.results.NS_BINDING_ABORTED);return;}
this.mDialog.dialog=this;this.getSpecialFolderKey=this.mDialog.getSpecialFolderKey;var progressListener=new nsUnknownContentTypeDialogProgressListener(this);this.mLauncher.setWebProgressListener(progressListener);},
displayBadPermissionAlert:function(){let bundle=Services.strings.createBundle("chrome://mozapps/locale/downloads/unknownContentType.properties");Services.prompt.alert(this.dialog,bundle.GetStringFromName("badPermissions.title"),bundle.GetStringFromName("badPermissions"));},promptForSaveToFileAsync:function(aLauncher,aContext,aDefaultFile,aSuggestedFileExtension,aForcePrompt){var result=null;this.mLauncher=aLauncher;let prefs=Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);let bundle=Services.strings.createBundle("chrome://mozapps/locale/downloads/unknownContentType.properties");let parent;let gDownloadLastDir;try{parent=aContext.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindow);}catch(ex){}
if(parent){gDownloadLastDir=new downloadModule.DownloadLastDir(parent);}else{

gDownloadLastDir=this._mDownloadDir;let windowsEnum=Services.wm.getEnumerator("");while(windowsEnum.hasMoreElements()){let someWin=windowsEnum.getNext();


if(someWin!=this.mDialog){parent=someWin;}}
if(!parent){Cu.reportError("No candidate parent windows were found for the save filepicker."+"This should never happen.");}}
Task.spawn(function(){if(!aForcePrompt){
let autodownload=false;try{autodownload=prefs.getBoolPref(PREF_BD_USEDOWNLOADDIR);}catch(e){}
if(autodownload){ let preferredDir=yield Downloads.getPreferredDownloadsDirectory();let defaultFolder=new FileUtils.File(preferredDir);try{result=this.validateLeafName(defaultFolder,aDefaultFile,aSuggestedFileExtension);}
catch(ex){} 
if(result){aLauncher.saveDestinationAvailable(result);return;}}}
var nsIFilePicker=Components.interfaces.nsIFilePicker;var picker=Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);var windowTitle=bundle.GetStringFromName("saveDialogTitle");picker.init(parent,windowTitle,nsIFilePicker.modeSave);picker.defaultString=aDefaultFile;if(aSuggestedFileExtension){ picker.defaultExtension=aSuggestedFileExtension.substring(1);}
else{try{picker.defaultExtension=this.mLauncher.MIMEInfo.primaryExtension;}
catch(ex){}}
var wildCardExtension="*";if(aSuggestedFileExtension){wildCardExtension+=aSuggestedFileExtension;picker.appendFilter(this.mLauncher.MIMEInfo.description,wildCardExtension);}
picker.appendFilters(nsIFilePicker.filterAll);

let preferredDir=yield Downloads.getPreferredDownloadsDirectory();picker.displayDirectory=new FileUtils.File(preferredDir);gDownloadLastDir.getFileAsync(aLauncher.source,function LastDirCallback(lastDir){if(lastDir&&isUsableDirectory(lastDir))
picker.displayDirectory=lastDir;if(picker.show()==nsIFilePicker.returnCancel){aLauncher.saveDestinationAvailable(null);return;}

result=picker.file;if(result){try{
 if(result.exists()&&this.getFinalLeafName(result.leafName)==result.leafName)
result.remove(false);}
catch(ex){
}
var newDir=result.parent.QueryInterface(Components.interfaces.nsILocalFile); gDownloadLastDir.setFile(aLauncher.source,newDir);try{result=this.validateLeafName(newDir,result.leafName,null);}
catch(ex){if(ex.result==Components.results.NS_ERROR_FILE_ACCESS_DENIED){this.displayBadPermissionAlert();aLauncher.saveDestinationAvailable(null);return;}}}
aLauncher.saveDestinationAvailable(result);}.bind(this));}.bind(this)).then(null,Components.utils.reportError);},getFinalLeafName:function(aLeafName,aFileExt)
{
aLeafName=aLeafName.replace(/^\.+/,"");if(aLeafName=="")
aLeafName="unnamed"+(aFileExt?"."+aFileExt:"");return aLeafName;},validateLeafName:function(aLocalFolder,aLeafName,aFileExt)
{if(!(aLocalFolder&&isUsableDirectory(aLocalFolder))){throw new Components.Exception("Destination directory non-existing or permission error",Components.results.NS_ERROR_FILE_ACCESS_DENIED);}
aLeafName=this.getFinalLeafName(aLeafName,aFileExt);aLocalFolder.append(aLeafName);
var createdFile=DownloadPaths.createNiceUniqueFile(aLocalFolder);if(AppConstants.platform=="win"){let ext;try{ ext="."+this.mLauncher.MIMEInfo.primaryExtension;}catch(e){}
 
let leaf=createdFile.leafName;if(ext&&leaf.slice(-ext.length)!=ext&&createdFile.isExecutable()){createdFile.remove(false);aLocalFolder.leafName=leaf+ext;createdFile=DownloadPaths.createNiceUniqueFile(aLocalFolder);}}
return createdFile;},initDialog:function(){var suggestedFileName=this.mLauncher.suggestedFileName;var url=this.mLauncher.source;if(url instanceof Components.interfaces.nsINestedURI)
url=url.innermostURI;var fname="";var iconPath="goat";this.mSourcePath=url.prePath;if(url instanceof Components.interfaces.nsIURL){fname=iconPath=url.fileName;this.mSourcePath+=url.directory;}else{fname=url.path;this.mSourcePath+=url.path;}
if(suggestedFileName)
fname=iconPath=suggestedFileName;var displayName=fname.replace(/ +/g," ");this.mTitle=this.dialogElement("strings").getFormattedString("title",[displayName]);this.mDialog.document.title=this.mTitle;this.initIntro(url,fname,displayName);var iconString="moz-icon://"+iconPath+"?size=16&contentType="+this.mLauncher.MIMEInfo.MIMEType;this.dialogElement("contentTypeImage").setAttribute("src",iconString);
 var mimeType=this.mLauncher.MIMEInfo.MIMEType;var shouldntRememberChoice=(mimeType=="application/octet-stream"||mimeType=="application/x-msdownload"||this.mLauncher.targetFileIsExecutable);if(shouldntRememberChoice&&!this.openWithDefaultOK()){ this.dialogElement("normalBox").collapsed=true; this.dialogElement("basicBox").collapsed=false;
 let acceptButton=this.mDialog.document.documentElement.getButton("accept");acceptButton.label=this.dialogElement("strings").getString("unknownAccept.label");acceptButton.setAttribute("icon","save");this.mDialog.document.documentElement.getButton("cancel").label=this.dialogElement("strings").getString("unknownCancel.label"); this.dialogElement("openHandler").collapsed=true; this.dialogElement("mode").selectedItem=this.dialogElement("save");}
else{this.initAppAndSaveToDiskValues();

var rememberChoice=this.dialogElement("rememberChoice");








if(shouldntRememberChoice){rememberChoice.checked=false;rememberChoice.disabled=true;}
else{rememberChoice.checked=!this.mLauncher.MIMEInfo.alwaysAskBeforeHandling&&this.mLauncher.MIMEInfo.preferredAction!=this.nsIMIMEInfo.handleInternally;}
this.toggleRememberChoice(rememberChoice);var openHandler=this.dialogElement("openHandler");openHandler.parentNode.removeChild(openHandler);var openHandlerBox=this.dialogElement("openHandlerBox");openHandlerBox.appendChild(openHandler);}
this.mDialog.setTimeout("dialog.postShowCallback()",0);this.delayHelper=new EnableDelayHelper({disableDialog:()=>{this.mDialog.document.documentElement.getButton("accept").disabled=true;},enableDialog:()=>{this.mDialog.document.documentElement.getButton("accept").disabled=false;},focusTarget:this.mDialog});},notify:function(aTimer){if(aTimer==this._showTimer){if(!this.mDialog){this.reallyShow();}
this._showTimer=null;}
else if(aTimer==this._saveToDiskTimer){this.mLauncher.saveToDisk(null,false);this._saveToDiskTimer=null;}},postShowCallback:function(){this.mDialog.sizeToContent(); this.dialogElement("mode").focus();},initIntro:function(url,filename,displayname){this.dialogElement("location").value=displayname;this.dialogElement("location").setAttribute("realname",filename);this.dialogElement("location").setAttribute("tooltiptext",displayname);
var pathString;if(url instanceof Components.interfaces.nsIFileURL){try{ pathString=url.file.parent.path;}catch(ex){}}
if(!pathString){ var tmpurl=url.clone(); try{tmpurl.userPass="";}catch(ex){}
pathString=tmpurl.prePath;} 
var location=this.dialogElement("source");location.value=pathString;location.setAttribute("tooltiptext",this.mSourcePath);var type=this.dialogElement("type");var mimeInfo=this.mLauncher.MIMEInfo;var typeString=mimeInfo.description;if(typeString==""){var primaryExtension="";try{primaryExtension=mimeInfo.primaryExtension;}
catch(ex){}
if(primaryExtension!="")
typeString=this.dialogElement("strings").getFormattedString("fileType",[primaryExtension.toUpperCase()]);else
typeString=mimeInfo.MIMEType;} 
if(this.mLauncher.contentLength>=0){let[size,unit]=DownloadUtils.convertByteUnits(this.mLauncher.contentLength);type.value=this.dialogElement("strings").getFormattedString("orderedFileSizeWithType",[typeString,size,unit]);}
else{type.value=typeString;}},openWithDefaultOK:function(){if(AppConstants.platform=="win"){



return!this.mLauncher.targetFileIsExecutable;}

return this.mLauncher.MIMEInfo.hasDefaultHandler;},initDefaultApp:function(){var desc=this.mLauncher.MIMEInfo.defaultDescription;if(desc){var defaultApp=this.dialogElement("strings").getFormattedString("defaultApp",[desc]);this.dialogElement("defaultHandler").label=defaultApp;}
else{this.dialogElement("modeDeck").setAttribute("selectedIndex","1");
this.dialogElement("defaultHandler").hidden=true;}},getPath:function(aFile){if(AppConstants.platform=="macosx"){return aFile.leafName||aFile.path;}
return aFile.path;},initAppAndSaveToDiskValues:function(){var modeGroup=this.dialogElement("mode");
var openWithDefaultOK=this.openWithDefaultOK();var mimeType=this.mLauncher.MIMEInfo.MIMEType;if(this.mLauncher.targetFileIsExecutable||((mimeType=="application/octet-stream"||mimeType=="application/x-msdownload")&&!openWithDefaultOK)){this.dialogElement("open").disabled=true;var openHandler=this.dialogElement("openHandler");openHandler.disabled=true;openHandler.selectedItem=null;modeGroup.selectedItem=this.dialogElement("save");return;}
try{this.chosenApp=this.mLauncher.MIMEInfo.preferredApplicationHandler.QueryInterface(Components.interfaces.nsILocalHandlerApp);}catch(e){this.chosenApp=null;}
this.initDefaultApp();var otherHandler=this.dialogElement("otherHandler");if(this.chosenApp&&this.chosenApp.executable&&this.chosenApp.executable.path){otherHandler.setAttribute("path",this.getPath(this.chosenApp.executable));otherHandler.label=this.getFileDisplayName(this.chosenApp.executable);otherHandler.hidden=false;}
var openHandler=this.dialogElement("openHandler");openHandler.selectedIndex=0;var defaultOpenHandler=this.dialogElement("defaultHandler");if(this.mLauncher.MIMEInfo.preferredAction==this.nsIMIMEInfo.useSystemDefault){modeGroup.selectedItem=this.dialogElement("open");}else if(this.mLauncher.MIMEInfo.preferredAction==this.nsIMIMEInfo.useHelperApp){modeGroup.selectedItem=this.dialogElement("open");openHandler.selectedItem=(otherHandler&&!otherHandler.hidden)?otherHandler:defaultOpenHandler;}else{modeGroup.selectedItem=this.dialogElement("save");}
if(!openWithDefaultOK){var isSelected=defaultOpenHandler.selected;defaultOpenHandler.hidden=true;if(isSelected){openHandler.selectedIndex=1;modeGroup.selectedItem=this.dialogElement("save");}}
otherHandler.nextSibling.hidden=otherHandler.nextSibling.nextSibling.hidden=false;this.updateOKButton();}, helperAppChoice:function(){return this.chosenApp;},get saveToDisk(){return this.dialogElement("save").selected;},get useOtherHandler(){return this.dialogElement("open").selected&&this.dialogElement("openHandler").selectedIndex==1;},get useSystemDefault(){return this.dialogElement("open").selected&&this.dialogElement("openHandler").selectedIndex==0;},toggleRememberChoice:function(aCheckbox){this.dialogElement("settingsChange").hidden=!aCheckbox.checked;this.mDialog.sizeToContent();},openHandlerCommand:function(){var openHandler=this.dialogElement("openHandler");if(openHandler.selectedItem.id=="choose")
this.chooseApp();else
openHandler.setAttribute("lastSelectedItemID",openHandler.selectedItem.id);},updateOKButton:function(){var ok=false;if(this.dialogElement("save").selected){ok=true;}
else if(this.dialogElement("open").selected){switch(this.dialogElement("openHandler").selectedIndex){case 0:ok=true;break;case 1:
ok=this.chosenApp||/\S/.test(this.dialogElement("otherHandler").getAttribute("path"));break;}}
this.mDialog.document.documentElement.getButton("accept").disabled=!ok;},appChanged:function(){return this.helperAppChoice()!=this.mLauncher.MIMEInfo.preferredApplicationHandler;},updateMIMEInfo:function(){

var discardUpdate=this.mLauncher.MIMEInfo.preferredAction==this.nsIMIMEInfo.handleInternally&&!this.dialogElement("rememberChoice").checked;var needUpdate=false;if(this.saveToDisk){needUpdate=this.mLauncher.MIMEInfo.preferredAction!=this.nsIMIMEInfo.saveToDisk;if(needUpdate)
this.mLauncher.MIMEInfo.preferredAction=this.nsIMIMEInfo.saveToDisk;}
else if(this.useSystemDefault){needUpdate=this.mLauncher.MIMEInfo.preferredAction!=this.nsIMIMEInfo.useSystemDefault;if(needUpdate)
this.mLauncher.MIMEInfo.preferredAction=this.nsIMIMEInfo.useSystemDefault;}
else{
needUpdate=this.mLauncher.MIMEInfo.preferredAction!=this.nsIMIMEInfo.useHelperApp||this.appChanged();if(needUpdate){this.mLauncher.MIMEInfo.preferredAction=this.nsIMIMEInfo.useHelperApp; var app=this.helperAppChoice();this.mLauncher.MIMEInfo.preferredApplicationHandler=app;}}
needUpdate=needUpdate||this.mLauncher.MIMEInfo.alwaysAskBeforeHandling!=(!this.dialogElement("rememberChoice").checked);




needUpdate=needUpdate||!this.mLauncher.MIMEInfo.alwaysAskBeforeHandling;this.mLauncher.MIMEInfo.alwaysAskBeforeHandling=!this.dialogElement("rememberChoice").checked;return needUpdate&&!discardUpdate;},
updateHelperAppPref:function(){var ha=new this.mDialog.HelperApps();ha.updateTypeInfo(this.mLauncher.MIMEInfo);ha.destroy();},onOK:function(){if(this.useOtherHandler){var helperApp=this.helperAppChoice();if(!helperApp||!helperApp.executable||!helperApp.executable.exists()){var bundle=this.dialogElement("strings");var msg=bundle.getFormattedString("badApp",[this.dialogElement("otherHandler").getAttribute("path")]);Services.prompt.alert(this.mDialog,bundle.getString("badApp.title"),msg);this.mDialog.document.documentElement.getButton("accept").disabled=true;this.dialogElement("mode").focus();this.chosenApp=null;return false;}}

this.mLauncher.setWebProgressListener(null);


try{var needUpdate=this.updateMIMEInfo();if(this.dialogElement("save").selected){


 this._saveToDiskTimer=Components.classes["@mozilla.org/timer;1"].createInstance(nsITimer);this._saveToDiskTimer.initWithCallback(this,0,nsITimer.TYPE_ONE_SHOT);}
else
this.mLauncher.launchWithApplication(null,false);



if(needUpdate&&this.mLauncher.MIMEInfo.MIMEType!="application/octet-stream")
this.updateHelperAppPref();}catch(e){}
this.mDialog.dialog=null;return true;},onCancel:function(){this.mLauncher.setWebProgressListener(null);try{this.mLauncher.cancel(Components.results.NS_BINDING_ABORTED);}catch(exception){}
this.mDialog.dialog=null;return true;},dialogElement:function(id){return this.mDialog.document.getElementById(id);}, getFileDisplayName:function getFileDisplayName(file)
{if(AppConstants.platform=="win"){if(file instanceof Components.interfaces.nsILocalFileWin){try{return file.getVersionInfoField("FileDescription");}catch(e){}}}else if(AppConstants.platform=="macosx"){if(file instanceof Components.interfaces.nsILocalFileMac){try{return file.bundleDisplayName;}catch(e){}}}
return file.leafName;},finishChooseApp:function(){if(this.chosenApp){
this.dialogElement("modeDeck").setAttribute("selectedIndex","0");var otherHandler=this.dialogElement("otherHandler");otherHandler.removeAttribute("hidden");otherHandler.setAttribute("path",this.getPath(this.chosenApp.executable));if(AppConstants.platform=="win")
otherHandler.label=this.getFileDisplayName(this.chosenApp.executable);else
otherHandler.label=this.chosenApp.name;this.dialogElement("openHandler").selectedIndex=1;this.dialogElement("openHandler").setAttribute("lastSelectedItemID","otherHandler");this.dialogElement("mode").selectedItem=this.dialogElement("open");}
else{var openHandler=this.dialogElement("openHandler");var lastSelectedID=openHandler.getAttribute("lastSelectedItemID");if(!lastSelectedID)
lastSelectedID="defaultHandler";openHandler.selectedItem=this.dialogElement(lastSelectedID);}},chooseApp:function(){if(AppConstants.platform=="win"){ var fileExtension="";try{fileExtension=this.mLauncher.MIMEInfo.primaryExtension;}catch(ex){}
var typeString=this.mLauncher.MIMEInfo.description;if(!typeString){
if(fileExtension){typeString=this.dialogElement("strings").getFormattedString("fileType",[fileExtension.toUpperCase()]);}else{typeString=this.mLauncher.MIMEInfo.MIMEType;}}
var params={};params.title=this.dialogElement("strings").getString("chooseAppFilePickerTitle");params.description=typeString;params.filename=this.mLauncher.suggestedFileName;params.mimeInfo=this.mLauncher.MIMEInfo;params.handlerApp=null;this.mDialog.openDialog("chrome://global/content/appPicker.xul",null,"chrome,modal,centerscreen,titlebar,dialog=yes",params);if(params.handlerApp&&params.handlerApp.executable&&params.handlerApp.executable.isFile()){this.chosenApp=params.handlerApp;}}
else{var nsIFilePicker=Components.interfaces.nsIFilePicker;var fp=Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);fp.init(this.mDialog,this.dialogElement("strings").getString("chooseAppFilePickerTitle"),nsIFilePicker.modeOpen);fp.appendFilters(nsIFilePicker.filterApps);if(fp.show()==nsIFilePicker.returnOK&&fp.file){var localHandlerApp=Components.classes["@mozilla.org/uriloader/local-handler-app;1"].createInstance(Components.interfaces.nsILocalHandlerApp);localHandlerApp.executable=fp.file;this.chosenApp=localHandlerApp;}
}
this.finishChooseApp();},debug:false,dump:function(text){if(this.debug){dump(text);}},dumpObj:function(spec){var val="<undefined>";try{val=eval("this."+spec).toString();}catch(exception){}
this.dump(spec+"="+val+"\n");}, dumpObjectProperties:function(desc,obj){for(prop in obj){this.dump(desc+"."+prop+"=");var val="<undefined>";try{val=obj[prop];}catch(exception){}
this.dump(val+"\n");}}}
this.NSGetFactory=XPCOMUtils.generateNSGetFactory([nsUnknownContentTypeDialog]);