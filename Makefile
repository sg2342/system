# Project Pris/GerdaOS main makefile

# Common vars

PERSISTDIR = '/data/media/0'
INTERNALDEVICE = '/dev/block/bootdevice/by-name/userdata'
SHELL = '/bin/bash'
BASE_SYSTEM_SIZE = 838860800
BUILDID = $(shell bash -c 'echo $$RANDOM')

TARGET = '8110'

VERSION := $(or $(VERSION),$(shell git rev-parse --short HEAD))

# Common tasks

all: build backup deploy

build: build-boot build-splash build-system

backup: backup-boot backup-splash backup-system backup-modem backup-radio

deploy: deploy-boot deploy-splash deploy-system deploy-modem deploy-radio

clean: clean-boot clean-splash clean-system clean-installer

# Boot image part

pre-build-boot: clean-boot
	mkdir -p ./works/boot
	cp -R ./src/boot/${TARGET}/* ./works/boot/

build-boot: pre-build-boot
	cd ./tools/abootimg && make
	cd ./works/boot/root && chmod -R 775 sbin && find . -print0 | cpio -o0a -H newc -R +0:+0 | gzip -9 > ../ramdisk.new.img
	cd ./works/boot && ../../tools/abootimg/abootimg --create ../../boot.new.img -f bootimg.cfg -k zImage -r ramdisk.new.img
	echo 'Boot image created at boot.new.img'

backup-boot:
	adb shell mount -o nosuid,nodev,noatime,barrier=1,noauto_da_alloc,discard ${INTERNALDEVICE} /data
	adb shell dd of=${PERSISTDIR}/boot.backup.img if=/dev/block/bootdevice/by-name/boot bs=2048
	mkdir -p works/backups && adb pull ${PERSISTDIR}/boot.backup.img works/backups/boot.${BUILDID}.img
	adb shell rm -f ${PERSISTDIR}/boot.backup.img
	echo "Boot image backed up to works/backups/boot.${BUILDID}.img"

deploy-boot:
	adb shell mount -o nosuid,nodev,noatime,barrier=1,noauto_da_alloc,discard ${INTERNALDEVICE} /data
	adb push ./boot.new.img ${PERSISTDIR}/
	adb shell dd if=${PERSISTDIR}/boot.new.img of=/dev/block/bootdevice/by-name/boot bs=2048
	adb shell rm ${PERSISTDIR}/boot.new.img
	echo 'Boot image deployed'

clean-boot:
	cd ./tools/abootimg && make clean
	rm -rf ./works/boot
	rm -f ./boot.new.img 

# System image part

pre-build-system: clean-system
	cd ./tools/make_ext4fs && make clean && make && cd ../../
	mkdir -p ./works/system
	cp -a ./src/system/* ./works/system/
	chmod 755 ./works/system/b2g/b2g
	chmod 755 ./works/system/kaios/api-daemon
	chmod 755 ./works/system/vendor/bin/*
	chmod 755 ./works/system/vendor/picolisp/bin/picolisp
	echo "Updating adblock hosts..."
	curl https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling/hosts > ./works/system/etc/hosts
	echo "0.0.0.0 ssp.kaiads.com" >> ./works/system/etc/hosts

repack-apps: pre-build-system
	echo "Packaging omni.ja";
	cd ./works/system/b2g/omni_src && zip -9 ../omni.ja -r .
	rm -Rf ./works/system/b2g/omni_src
	echo "Packaging boot animation";
	rm -f ./works/system/media/bootanimation.zip
	cd ./works/system/media/bootanimation_src && zip -0qry -i \*.txt \*.png \*.wav @ ../bootanimation.zip ./*
	rm -Rf ./works/system/media/bootanimation_src
	echo "Packaging WebApps";
	cd ./works/system/b2g/webapps && find . -type d -maxdepth 1 -mindepth 1 | while read WEBAPP;\
        do\
          echo "Packaging $$WEBAPP";\
          if [ -d "$$WEBAPP/src" ]; then\
            cd "$$WEBAPP/src";\
            zip -9 ../application.zip -r .;\
            cp manifest.webapp ../;\
            cd -;\
            rm -Rf "$$WEBAPP/src";\
          fi;\
        done;

build-system: repack-apps
	./tools/make_ext4fs/make_ext4fs -a '/system' -L 'system' -j 0 -l ${BASE_SYSTEM_SIZE} ./system.new.img ./works/system/
	echo 'System image created at system.new.img'

backup-system:
	adb shell mount -o nosuid,nodev,noatime,barrier=1,noauto_da_alloc,discard ${INTERNALDEVICE} /data
	adb shell dd of=${PERSISTDIR}/system.backup.img if=/dev/block/bootdevice/by-name/system bs=2048
	mkdir -p works/backups && adb pull ${PERSISTDIR}/system.backup.img works/backups/system.${BUILDID}.img
	adb shell rm -f ${PERSISTDIR}/system.backup.img
	echo "System image backed up to works/backups/system.${BUILDID}.img"

deploy-system:
	adb shell mount -o nosuid,nodev,noatime,barrier=1,noauto_da_alloc,discard ${INTERNALDEVICE} /data
	adb push ./system.new.img ${PERSISTDIR}/
	adb shell dd if=${PERSISTDIR}/system.new.img of=/dev/block/bootdevice/by-name/system bs=2048
	adb shell rm ${PERSISTDIR}/system.new.img
	echo 'System image deployed'

clean-system:
	rm -f ./system.new.img
	rm -rf ./works/system
	cd ./tools/make_ext4fs && make clean

# Splash image part

build-splash: clean-splash
	ffmpeg -vcodec png -i src/splash/logo.png -vcodec rawvideo -f rawvideo -pix_fmt bgr565 -s 240x320 -y ./tmp2.bin
	cat ./src/splash/logohdr.bin ./tmp2.bin > ./splash.new.img
	rm -f ./tmp2.bin
	echo 'Splash image created at splash.new.img'

backup-splash:
	adb shell mount -o nosuid,nodev,noatime,barrier=1,noauto_da_alloc,discard ${INTERNALDEVICE} /data
	adb shell dd of=${PERSISTDIR}/splash.backup.img if=/dev/block/bootdevice/by-name/splash bs=2048
	mkdir -p works/backups && adb pull ${PERSISTDIR}/splash.backup.img works/backups/splash.${BUILDID}.img
	adb shell rm -f ${PERSISTDIR}/splash.backup.img
	echo "Splash image backed up to works/backups/splash.${BUILDID}.img"

deploy-splash:
	adb shell mount -o nosuid,nodev,noatime,barrier=1,noauto_da_alloc,discard ${INTERNALDEVICE} /data
	adb push ./splash.new.img ${PERSISTDIR}/
	adb shell dd if=${PERSISTDIR}/splash.new.img of=/dev/block/bootdevice/by-name/splash bs=2048
	adb shell rm ${PERSISTDIR}/splash.new.img
	echo 'Splash image deployed'

clean-splash:
	rm -f ./splash.new.img

# Modem image part

backup-modem:
	adb shell mount -o nosuid,nodev,noatime,barrier=1,noauto_da_alloc,discard ${INTERNALDEVICE} /data
	adb shell dd of=${PERSISTDIR}/modem.backup.img if=/dev/block/bootdevice/by-name/modem bs=2048
	mkdir -p works/backups && adb pull ${PERSISTDIR}/modem.backup.img works/backups/modem.${BUILDID}.img
	adb shell rm -f ${PERSISTDIR}/modem.backup.img
	echo "Modem image backed up to works/backups/modem.${BUILDID}.img"

deploy-modem:
	adb shell mount -o nosuid,nodev,noatime,barrier=1,noauto_da_alloc,discard ${INTERNALDEVICE} /data
	adb push blobs/${TARGET}/modem.img ${PERSISTDIR}/
	adb shell dd if=${PERSISTDIR}/modem.img of=/dev/block/bootdevice/by-name/modem bs=2048
	adb shell rm ${PERSISTDIR}/modem.img
	echo 'Modem image deployed'

# radio part

backup-radio:
	adb shell mount -o nosuid,nodev,noatime,barrier=1,noauto_da_alloc,discard ${INTERNALDEVICE} /data
	adb shell dd of=${PERSISTDIR}/fsg.backup.img if=/dev/block/bootdevice/by-name/fsg bs=2048
	mkdir -p works/backups && adb pull ${PERSISTDIR}/fsg.backup.img works/backups/fsg.${BUILDID}.img
	adb shell rm -f ${PERSISTDIR}/fsg.backup.img
	adb shell dd of=${PERSISTDIR}/modemst1.backup.img if=/dev/block/bootdevice/by-name/modemst1 bs=2048
	mkdir -p works/backups && adb pull ${PERSISTDIR}/modemst1.backup.img works/backups/modemst1.${BUILDID}.img
	adb shell rm -f ${PERSISTDIR}/modemst1.backup.img
	adb shell dd of=${PERSISTDIR}/modemst2.backup.img if=/dev/block/bootdevice/by-name/modemst2 bs=2048
	mkdir -p works/backups && adb pull ${PERSISTDIR}/modemst2.backup.img works/backups/modemst2.${BUILDID}.img
	adb shell rm -f ${PERSISTDIR}/modemst2.backup.img
	adb shell dd of=${PERSISTDIR}/rpm.backup.img if=/dev/block/bootdevice/by-name/rpm bs=2048
	mkdir -p works/backups && adb pull ${PERSISTDIR}/rpm.backup.img works/backups/rpm.${BUILDID}.img
	adb shell rm -f ${PERSISTDIR}/rpm.backup.img
	echo "Radio images backed up to works/backups/{fsg,modemst1,modemst2,rpm}.${BUILDID}.img"

deploy-radio:
	adb shell mount -o nosuid,nodev,noatime,barrier=1,noauto_da_alloc,discard ${INTERNALDEVICE} /data
	adb push blobs/${TARGET}/fsg.bin ${PERSISTDIR}/
	adb shell dd if=${PERSISTDIR}/fsg.bin of=/dev/block/bootdevice/by-name/fsg bs=2048
	adb shell dd if=/dev/zero of=/dev/block/bootdevice/by-name/modemst1 bs=2048
	adb shell dd if=/dev/zero of=/dev/block/bootdevice/by-name/modemst2 bs=2048
	adb shell rm ${PERSISTDIR}/fsg.bin
	adb push blobs/${TARGET}/rpm.img ${PERSISTDIR}/
	adb shell dd if=${PERSISTDIR}/rpm.img of=/dev/block/bootdevice/by-name/rpm bs=2048
	adb shell rm ${PERSISTDIR}/rpm.img
	echo 'Radio images deployed'

# installer part

build-installer: clean build-system build-boot build-splash
	mkdir -p ./works/installer
	cp -r ./src/installer/* ./works/installer/
	mv ./system.new.img ./works/installer/
	mv ./boot.new.img ./works/installer/
	mv ./splash.new.img ./works/installer/
	cp ./blobs/${TARGET}/modem.img ./works/installer/
	cp ./blobs/${TARGET}/fsg.bin ./works/installer/
	cp ./blobs/${TARGET}/rpm.img ./works/installer/
	./tools/stockerize/stockerize.sh ./works/installer ./gerda-install-${VERSION}.zip && rm -rf ./works/installer

clean-installer:
	rm -rf ./gerda-install*.zip
